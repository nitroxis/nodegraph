﻿namespace NodeGraph
{
	/// <summary>
	/// Represents an interface for lazy-loading values.
	/// </summary>
	public interface ILazy
	{
		void Invalidate();
	}

	/// <summary>
	/// Represents an abstract class for lazy-loading values.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public abstract class Lazy<T> : ILazy
	{
		#region Fields

		private T value;
		private bool isInvalid;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the value.
		/// </summary>
		public T Value
		{
			get
			{
				if (this.isInvalid)
				{
					this.value = this.Request();
					this.isInvalid = false;
				}
				return this.value;
			}
		}

		#endregion

		#region Constructors

		#endregion

		#region Methods

		/// <summary>
		/// Requests the value.
		/// </summary>
		/// <returns></returns>
		protected abstract T Request();

		/// <summary>
		/// Invalidates the value.
		/// </summary>
		/// <returns></returns>
		public void Invalidate()
		{
			this.isInvalid = true;
		}

		#endregion
	}
}

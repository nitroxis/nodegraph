﻿using System;

namespace NodeGraph
{
	/// <summary>
	/// Represents EventArgs that contain a node.
	/// </summary>
	public class NodeEventArgs : EventArgs
	{
		#region Fields

		private readonly Node node;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the node instance.
		/// </summary>
		public Node Node
		{
			get { return this.node; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new NodeEventArgs.
		/// </summary>
		public NodeEventArgs(Node node)
		{
			this.node = node;
		}

		#endregion

		#region Methods

		#endregion
	}
}

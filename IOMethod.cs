﻿using System.Reflection.Emit;

namespace NodeGraph
{
	/// <summary>
	/// Defines IO methods for loading inputs and storing outputs.
	/// </summary>
	public enum IOMethod
	{
		/// <summary>
		/// Direct argument value access.
		/// </summary>
		Arg,

		/// <summary>
		/// Accesses the value of an object array.
		/// </summary>
		Dynamic,

		/// <summary>
		/// Accesses the value from a local.
		/// </summary>
		Embedded,
	}
}

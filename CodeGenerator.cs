﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;

namespace NodeGraph
{
	/// <summary>
	/// Represents an abstract code generator.
	/// </summary>
	public sealed class CodeGenerator
	{
		#region Fields

		private readonly ILGenerator il;
		private IOMethod io;
		private readonly List<LocalBuilder> tempLocals;
		private readonly GraphState state;
		private readonly List<Node> processedNodes;

		private readonly Dictionary<Output, LocalBuilder> outputLocals;
		private readonly Dictionary<Output, int> outputStates;
		
		#endregion

		#region Properties

		public ILGenerator IL
		{
			get { return this.il; }	
		}

		public IOMethod IO
		{
			get { return this.io; }
			set { this.io = value; }
		}

		public GraphState State
		{
			get { return this.state; }
		}

		public List<Node> ProcessedNodes
		{
			get { return this.processedNodes; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Context.
		/// </summary>
		public CodeGenerator(ILGenerator il, IOMethod io)
		{
			this.il = il;
			this.io = io;
			this.tempLocals = new List<LocalBuilder>();
			this.state = new GraphState();
			this.processedNodes = new List<Node>();
			this.outputStates = new Dictionary<Output, int>();
			this.outputLocals = new Dictionary<Output, LocalBuilder>();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Loads all inputs of the specified node onto the evaluation stack.
		/// </summary>
		/// <param name="node"></param>
		public void LoadInputs(Node node)
		{
			this.LoadInputs(node.GetInputs());
		}

		/// <summary>
		/// Loads the specified inputs onto the evaluation stack.
		/// </summary>
		/// <param name="inputs"></param>
		public void LoadInputs(IEnumerable<Input> inputs)
		{
			foreach (Input input in inputs)
				this.LoadInput(input);
		}

		/// <summary>
		/// Loads an input onto the evaluation stack.
		/// </summary>
		/// <param name="input"></param>
		public void LoadInput(Input input)
		{
			Output source = input.GetSource();
			if (source == null)
			{
				// load null.
				if (input.Type == typeof(bool) || input.Type == typeof(char) ||
					input.Type == typeof(byte) || input.Type == typeof(sbyte) ||
					input.Type == typeof(short) || input.Type == typeof(ushort) ||
					input.Type == typeof(int) || input.Type == typeof(uint))
					this.il.Emit(OpCodes.Ldc_I4_0);
				else if (input.Type == typeof(long) || input.Type == typeof(ulong))
					this.il.Emit(OpCodes.Ldc_I8, 0L);
				else if (input.Type == typeof(float))
					this.il.Emit(OpCodes.Ldc_R4, 0.0f);
				else if (input.Type == typeof(double))
					this.il.Emit(OpCodes.Ldc_R8, 0.0d);
				else
					this.il.Emit(OpCodes.Ldnull);

				return;
			}

			if (this.outputLocals.ContainsKey(source))
			{
				// load from local.
				LocalBuilder local;
				if (!this.outputLocals.TryGetValue(source, out local))
					throw new Exception("Output local not defined.");

				this.IL.EmitLdloc(local.LocalIndex);
			}
			else if (this.outputStates.ContainsKey(source))
			{
				// load from state.
				this.LoadState(input.Type, this.outputStates[source]);
			}
			else
			{
				throw new InvalidOperationException();
			}
		}

		public void LoadState(Type type, int index)
		{
			if (type == typeof(bool))
			{
				this.IL.Emit(OpCodes.Ldarg_0);
				this.IL.Emit(OpCodes.Ldfld, GraphState.BooleanField);
				this.IL.EmitLdc_I4(index);
				this.IL.Emit(OpCodes.Ldelem_I1);
			}
			else if (type == typeof(sbyte))
			{
				this.IL.Emit(OpCodes.Ldarg_0);
				this.IL.Emit(OpCodes.Ldfld, GraphState.Int8Field);
				this.IL.EmitLdc_I4(index);
				this.IL.Emit(OpCodes.Ldelem_I1);
			}
			else if (type == typeof(byte))
			{
				this.IL.Emit(OpCodes.Ldarg_0);
				this.IL.Emit(OpCodes.Ldfld, GraphState.UInt8Field);
				this.IL.EmitLdc_I4(index);
				this.IL.Emit(OpCodes.Ldelem_U1);
			}
			else if (type == typeof(short))
			{
				this.IL.Emit(OpCodes.Ldarg_0);
				this.IL.Emit(OpCodes.Ldfld, GraphState.Int16Field);
				this.IL.EmitLdc_I4(index);
				this.IL.Emit(OpCodes.Ldelem_I2);
			}
			else if (type == typeof(ushort))
			{
				this.IL.Emit(OpCodes.Ldarg_0);
				this.IL.Emit(OpCodes.Ldfld, GraphState.UInt16Field);
				this.IL.EmitLdc_I4(index);
				this.IL.Emit(OpCodes.Ldelem_U2);
			}
			else if (type == typeof(int))
			{
				this.IL.Emit(OpCodes.Ldarg_0);
				this.IL.Emit(OpCodes.Ldfld, GraphState.Int32Field);
				this.IL.EmitLdc_I4(index);
				this.IL.Emit(OpCodes.Ldelem_I4);
			}
			else if (type == typeof(uint))
			{
				this.IL.Emit(OpCodes.Ldarg_0);
				this.IL.Emit(OpCodes.Ldfld, GraphState.UInt32Field);
				this.IL.EmitLdc_I4(index);
				this.IL.Emit(OpCodes.Ldelem_U4);
			}
			else if (type == typeof(long))
			{
				this.IL.Emit(OpCodes.Ldarg_0);
				this.IL.Emit(OpCodes.Ldfld, GraphState.Int64Field);
				this.IL.EmitLdc_I4(index);
				this.IL.Emit(OpCodes.Ldelem_I8);
			}
			else if (type == typeof(ulong))
			{
				this.IL.Emit(OpCodes.Ldarg_0);
				this.IL.Emit(OpCodes.Ldfld, GraphState.UInt64Field);
				this.IL.EmitLdc_I4(index);
				this.IL.Emit(OpCodes.Ldelem_I8);
			}
			else if (type == typeof(float))
			{
				this.IL.Emit(OpCodes.Ldarg_0);
				this.IL.Emit(OpCodes.Ldfld, GraphState.SingleField);
				this.IL.EmitLdc_I4(index);
				this.IL.Emit(OpCodes.Ldelem_R4);
			}
			else if (type == typeof(double))
			{
				this.IL.Emit(OpCodes.Ldarg_0);
				this.IL.Emit(OpCodes.Ldfld, GraphState.DoubleField);
				this.IL.EmitLdc_I4(index);
				this.IL.Emit(OpCodes.Ldelem_R8);
			}
			else if (type == typeof(char))
			{
				this.IL.Emit(OpCodes.Ldarg_0);
				this.IL.Emit(OpCodes.Ldfld, GraphState.CharField);
				this.IL.EmitLdc_I4(index);
				this.IL.Emit(OpCodes.Ldelem_U2);
			}
			else if (type == typeof(string))
			{
				this.IL.Emit(OpCodes.Ldarg_0);
				this.IL.Emit(OpCodes.Ldfld, GraphState.StringField);
				this.IL.EmitLdc_I4(index);
				this.IL.Emit(OpCodes.Ldelem_Ref);
			}
			else
			{
				this.IL.Emit(OpCodes.Ldarg_0);
				this.IL.Emit(OpCodes.Ldfld, GraphState.ObjectField);
				this.IL.EmitLdc_I4(index);
				this.IL.Emit(OpCodes.Ldelem_Ref);
				if (type.IsValueType)
					this.IL.Emit(OpCodes.Unbox_Any, type);
				else if (type != typeof(object))
					this.IL.Emit(OpCodes.Castclass, type);
			}
		}


		/// <summary>
		/// Creates storage for the specified output.
		/// </summary>
		/// <param name="output"></param>
		public void CreateOutputStorage(Output output)
		{
			if (output.IsCyclic)
			{
				int index = this.AddState(output.Type);
				this.outputStates.Add(output, index);
			}
			else
			{
				LocalBuilder local = this.IL.DeclareLocal(output.Type);
				this.outputLocals.Add(output, local);
			}
		}

		public int AddState(Type type)
		{
			FieldInfo field;
			int index;
			this.AddState(type, out index, out field);
			return index;
		}

		public void AddState(Type type, out int index, out FieldInfo field)
		{
			if (type == typeof(bool))
			{
				index = this.State.AddBoolean(false);
				field = GraphState.BooleanField;
			}
			else if (type == typeof(sbyte))
			{
				index = this.State.AddInt8(0);
				field = GraphState.Int8Field;
			}
			else if (type == typeof(byte))
			{
				index = this.State.AddUInt8(0);
				field = GraphState.UInt8Field;
			}
			else if (type == typeof(short))
			{
				index = this.State.AddInt16(0);
				field = GraphState.Int16Field;
			}
			else if (type == typeof(ushort))
			{
				index = this.State.AddUInt16(0);
				field = GraphState.UInt16Field;
			}
			else if (type == typeof(int))
			{
				index = this.State.AddInt32(0);
				field = GraphState.Int32Field;
			}
			else if (type == typeof(uint))
			{
				index = this.State.AddUInt32(0);
				field = GraphState.UInt32Field;
			}
			else if (type == typeof(long))
			{
				index = this.State.AddInt64(0);
				field = GraphState.Int64Field;
			}
			else if (type == typeof(ulong))
			{
				index = this.State.AddUInt64(0);
				field = GraphState.UInt64Field;
			}
			else if (type == typeof(float))
			{
				index = this.State.AddSingle(0);
				field = GraphState.SingleField;
			}
			else if (type == typeof(double))
			{
				index = this.State.AddDouble(0);
				field = GraphState.DoubleField;
			}
			else if (type == typeof(char))
			{
				index = this.State.AddChar('\0');
				field = GraphState.CharField;
			}
			else if (type == typeof(string))
			{
				index = this.State.AddString("");
				field = GraphState.StringField;
			}
			else
			{
				index = this.State.AddObject(null);
				field = GraphState.ObjectField;
			}
		}

		/// <summary>
		/// Stores the specified output.
		/// </summary>
		/// <param name="output"></param>
		public void StoreOutput(Output output)
		{
			if (this.outputLocals.ContainsKey(output))
				this.IL.EmitStloc(this.outputLocals[output].LocalIndex);
			else if (this.outputStates.ContainsKey(output))
				this.StoreState(output.Type, this.outputStates[output]);
			else
				throw new InvalidOperationException();
		}

		public void StoreState(Type type, int index)
		{
			LocalBuilder local = this.TakeTempLocal(type);
			this.IL.EmitStloc(local.LocalIndex);

			if (type == typeof(bool))
			{
				this.IL.Emit(OpCodes.Ldarg_0);
				this.IL.Emit(OpCodes.Ldfld, GraphState.BooleanField);
				this.IL.EmitLdc_I4(index);
				this.IL.EmitLdloc(local.LocalIndex);
				this.IL.Emit(OpCodes.Stelem_I1);
			}
			else if (type == typeof(sbyte))
			{
				this.IL.Emit(OpCodes.Ldarg_0);
				this.IL.Emit(OpCodes.Ldfld, GraphState.Int8Field);
				this.IL.EmitLdc_I4(index);
				this.IL.EmitLdloc(local.LocalIndex);
				this.IL.Emit(OpCodes.Stelem_I1);
			}
			else if (type == typeof(byte))
			{
				this.IL.Emit(OpCodes.Ldarg_0);
				this.IL.Emit(OpCodes.Ldfld, GraphState.UInt8Field);
				this.IL.EmitLdc_I4(index);
				this.IL.EmitLdloc(local.LocalIndex);
				this.IL.Emit(OpCodes.Stelem_I1);
			}
			else if (type == typeof(short))
			{
				this.IL.Emit(OpCodes.Ldarg_0);
				this.IL.Emit(OpCodes.Ldfld, GraphState.Int16Field);
				this.IL.EmitLdc_I4(index);
				this.IL.EmitLdloc(local.LocalIndex);
				this.IL.Emit(OpCodes.Stelem_I2);
			}
			else if (type == typeof(ushort))
			{
				this.IL.Emit(OpCodes.Ldarg_0);
				this.IL.Emit(OpCodes.Ldfld, GraphState.UInt16Field);
				this.IL.EmitLdc_I4(index);
				this.IL.EmitLdloc(local.LocalIndex);
				this.IL.Emit(OpCodes.Stelem_I2);
			}
			else if (type == typeof(int))
			{
				this.IL.Emit(OpCodes.Ldarg_0);
				this.IL.Emit(OpCodes.Ldfld, GraphState.Int32Field);
				this.IL.EmitLdc_I4(index);
				this.IL.EmitLdloc(local.LocalIndex);
				this.IL.Emit(OpCodes.Stelem_I4);
			}
			else if (type == typeof(uint))
			{
				this.IL.Emit(OpCodes.Ldarg_0);
				this.IL.Emit(OpCodes.Ldfld, GraphState.UInt32Field);
				this.IL.EmitLdc_I4(index);
				this.IL.EmitLdloc(local.LocalIndex);
				this.IL.Emit(OpCodes.Stelem_I4);
			}
			else if (type == typeof(long))
			{
				this.IL.Emit(OpCodes.Ldarg_0);
				this.IL.Emit(OpCodes.Ldfld, GraphState.Int64Field);
				this.IL.EmitLdc_I4(index);
				this.IL.EmitLdloc(local.LocalIndex);
				this.IL.Emit(OpCodes.Stelem_I8);
			}
			else if (type == typeof(ulong))
			{
				this.IL.Emit(OpCodes.Ldarg_0);
				this.IL.Emit(OpCodes.Ldfld, GraphState.UInt64Field);
				this.IL.EmitLdc_I4(index);
				this.IL.EmitLdloc(local.LocalIndex);
				this.IL.Emit(OpCodes.Stelem_I8);
			}
			else if (type == typeof(float))
			{
				this.IL.Emit(OpCodes.Ldarg_0);
				this.IL.Emit(OpCodes.Ldfld, GraphState.SingleField);
				this.IL.EmitLdc_I4(index);
				this.IL.EmitLdloc(local.LocalIndex);
				this.IL.Emit(OpCodes.Stelem_R4);
			}
			else if (type == typeof(double))
			{
				this.IL.Emit(OpCodes.Ldarg_0);
				this.IL.Emit(OpCodes.Ldfld, GraphState.DoubleField);
				this.IL.EmitLdc_I4(index);
				this.IL.EmitLdloc(local.LocalIndex);
				this.IL.Emit(OpCodes.Stelem_R8);
			}
			else if (type == typeof(char))
			{
				this.IL.Emit(OpCodes.Ldarg_0);
				this.IL.Emit(OpCodes.Ldfld, GraphState.CharField);
				this.IL.EmitLdc_I4(index);
				this.IL.EmitLdloc(local.LocalIndex);
				this.IL.Emit(OpCodes.Stelem_I2);
			}
			else if (type == typeof(string))
			{
				this.IL.Emit(OpCodes.Ldarg_0);
				this.IL.Emit(OpCodes.Ldfld, GraphState.StringField);
				this.IL.EmitLdc_I4(index);
				this.IL.EmitLdloc(local.LocalIndex);
				this.IL.Emit(OpCodes.Stelem_Ref);
			}
			else
			{
				this.IL.Emit(OpCodes.Ldarg_0);
				this.IL.Emit(OpCodes.Ldfld, GraphState.ObjectField);
				this.IL.EmitLdc_I4(index);
				this.IL.EmitLdloc(local.LocalIndex);
				if (type.IsValueType)
					this.IL.Emit(OpCodes.Box, type);
				this.IL.Emit(OpCodes.Stelem_Ref);
			}

			this.GiveTempLocal(local);
		}

		/// <summary>
		/// Returns a temporary local variable of the specified type from the pool.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public LocalBuilder TakeTempLocal(Type type)
		{
			foreach (LocalBuilder local in this.tempLocals)
			{
				if (local.LocalType == type)
				{
					this.tempLocals.Remove(local);
					return local;
				}
			}

			return this.il.DeclareLocal(type);
		}

		/// <summary>
		/// Adds a temporary local variable to the pool.
		/// </summary>
		/// <param name="local"></param>
		public void GiveTempLocal(LocalBuilder local)
		{
			this.tempLocals.Add(local);
		}

		#endregion
	}
}


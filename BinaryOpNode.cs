﻿using System.Reflection.Emit;

namespace NodeGraph
{
	/// <summary>
	/// Performs a binary IL operation. 
	/// </summary>
	public abstract class BinaryOpNode<TIn1, TIn2, TOut> : Node
	{
		#region Fields

		private readonly OpCode[] instructions;
		private readonly Input input1;
		private readonly Input input2;
		private readonly Output output;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the IL instructions that are performed.
		/// </summary>
		public OpCode[] Instructions
		{
			get { return this.instructions; }
		}

		/// <summary>
		/// First operand.
		/// </summary>
		public Input Input1
		{
			get { return this.input1; }
		}

		/// <summary>
		/// Second operand.
		/// </summary>
		public Input Input2
		{
			get { return this.input2; }
		}

		/// <summary>
		/// Result.
		/// </summary>
		public Output Output
		{
			get { return this.output; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new AddNode.
		/// </summary>
		protected BinaryOpNode(params OpCode[] instructions)
		{
			this.instructions = instructions;
			this.input1 = new Input(this, "Input 1", typeof(TIn1));
			this.input2 = new Input(this, "Input 2", typeof(TIn2));
			this.output = new Output(this, "Output", typeof(TOut));
		}

		#endregion

		#region Methods

		public override Input[] GetInputs()
		{
			return new[] { this.input1, this.input2 };
		}

		public override Output[] GetOutputs()
		{
			return new[] { this.output };
		}

		public override void Emit(CodeGenerator generator)
		{
			generator.LoadInput(this.input1);
			generator.LoadInput(this.input2);
			foreach(OpCode instruction in this.instructions)
				generator.IL.Emit(instruction);
			generator.StoreOutput(this.output);
		}

		#endregion
	}

	/// <summary>
	/// Performs a binary IL operation. 
	/// </summary>
	public abstract class BinaryOpNode<T> : BinaryOpNode<T, T, T>
	{
		#region Constructors

		/// <summary>
		/// Creates a new AddNode.
		/// </summary>
		protected BinaryOpNode(params OpCode[] instructions)
			: base(instructions)
		{

		}

		#endregion
	}

	#region Add

	public abstract class AddNode<T> : BinaryOpNode<T>
	{
		protected AddNode()
			: base(OpCodes.Add)
		{
			
		}
	}

	public sealed class Int8AddNode : AddNode<sbyte> { }
	public sealed class UInt8AddNode : AddNode<byte> { }

	public sealed class Int16AddNode : AddNode<short> { }
	public sealed class UInt16AddNode : AddNode<ushort> { }

	public sealed class Int32AddNode : AddNode<int> { }
	public sealed class UInt32AddNode : AddNode<uint> { }

	public sealed class Int64AddNode : AddNode<long> { }
	public sealed class UInt64AddNode : AddNode<ulong> { }

	public sealed class SingleAddNode : AddNode<float> { }
	public sealed class DoubleAddNode : AddNode<double> { }

	#endregion

	#region Subtract

	public abstract class SubtractNode<T> : BinaryOpNode<T>
	{
		protected SubtractNode()
			: base(OpCodes.Sub)
		{

		}
	}

	public sealed class Int8SubtractNode : SubtractNode<sbyte> { }
	public sealed class UInt8SubtractNode : SubtractNode<byte> { }

	public sealed class Int16SubtractNode : SubtractNode<short> { }
	public sealed class UInt16SubtractNode : SubtractNode<ushort> { }

	public sealed class Int32SubtractNode : SubtractNode<int> { }
	public sealed class UInt32SubtractNode : SubtractNode<uint> { }

	public sealed class Int64SubtractNode : SubtractNode<long> { }
	public sealed class UInt64SubtractNode : SubtractNode<ulong> { }

	public sealed class SingleSubtractNode : SubtractNode<float> { }
	public sealed class DoubleSubtractNode : SubtractNode<double> { }
	
	#endregion

	#region Multiply

	public abstract class MultiplyNode<T> : BinaryOpNode<T>
	{
		protected MultiplyNode()
			: base(OpCodes.Mul)
		{

		}
	}

	public sealed class Int8MultiplyNode : MultiplyNode<sbyte> { }
	public sealed class UInt8MultiplyNode : MultiplyNode<byte> { }

	public sealed class Int16MultiplyNode : MultiplyNode<short> { }
	public sealed class UInt16MultiplyNode : MultiplyNode<ushort> { }

	public sealed class Int32MultiplyNode : MultiplyNode<int> { }
	public sealed class UInt32MultiplyNode : MultiplyNode<uint> { }

	public sealed class Int64MultiplyNode : MultiplyNode<long> { }
	public sealed class UInt64MultiplyNode : MultiplyNode<ulong> { }

	public sealed class SingleMultiplyNode : MultiplyNode<float> { }
	public sealed class DoubleMultiplyNode : MultiplyNode<double> { }

	#endregion

	#region Divide

	public abstract class DivideNode<T> : BinaryOpNode<T>
	{
		protected DivideNode()
			: base(OpCodes.Div)
		{

		}
	}

	public sealed class Int8DivideNode : DivideNode<sbyte> { }
	public sealed class UInt8DivideNode : DivideNode<byte> { }

	public sealed class Int16DivideNode : DivideNode<short> { }
	public sealed class UInt16DivideNode : DivideNode<ushort> { }

	public sealed class Int32DivideNode : DivideNode<int> { }
	public sealed class UInt32DivideNode : DivideNode<uint> { }

	public sealed class Int64DivideNode : DivideNode<long> { }
	public sealed class UInt64DivideNode : DivideNode<ulong> { }

	public sealed class SingleDivideNode : DivideNode<float> { }
	public sealed class DoubleDivideNode : DivideNode<double> { }

	#endregion

	#region Modulo

	public abstract class ModuloNode<T> : BinaryOpNode<T>
	{
		protected ModuloNode()
			: base(OpCodes.Rem)
		{

		}
	}

	public sealed class Int8ModuloNode : ModuloNode<sbyte> { }
	public sealed class UInt8ModuloNode : ModuloNode<byte> { }

	public sealed class Int16ModuloNode : ModuloNode<short> { }
	public sealed class UInt16ModuloNode : ModuloNode<ushort> { }

	public sealed class Int32ModuloNode : ModuloNode<int> { }
	public sealed class UInt32ModuloNode : ModuloNode<uint> { }

	public sealed class Int64ModuloNode : ModuloNode<long> { }
	public sealed class UInt64ModuloNode : ModuloNode<ulong> { }

	public sealed class SingleModuloNode : ModuloNode<float> { }
	public sealed class DoubleModuloNode : ModuloNode<double> { }

	#endregion

	#region And

	public abstract class AndNode<T> : BinaryOpNode<T>
	{
		protected AndNode()
			: base(OpCodes.And)
		{

		}
	}

	public sealed class BooleanAndNode : AndNode<bool> { }

	public sealed class Int8AndNode : AndNode<sbyte> { }
	public sealed class UInt8AndNode : AndNode<byte> { }

	public sealed class Int16AndNode : AndNode<short> { }
	public sealed class UInt16AndNode : AndNode<ushort> { }

	public sealed class Int32AndNode : AndNode<int> { }
	public sealed class UInt32AndNode : AndNode<uint> { }

	public sealed class Int64AndNode : AndNode<long> { }
	public sealed class UInt64AndNode : AndNode<ulong> { }

	#endregion

	#region Or

	public abstract class OrNode<T> : BinaryOpNode<T>
	{
		protected OrNode()
			: base(OpCodes.Or)
		{

		}
	}

	public sealed class BooleanOrNode : OrNode<bool> { }

	public sealed class Int8OrNode : OrNode<sbyte> { }
	public sealed class UInt8OrNode : OrNode<byte> { }

	public sealed class Int16OrNode : OrNode<short> { }
	public sealed class UInt16OrNode : OrNode<ushort> { }

	public sealed class Int32OrNode : OrNode<int> { }
	public sealed class UInt32OrNode : OrNode<uint> { }

	public sealed class Int64OrNode : OrNode<long> { }
	public sealed class UInt64OrNode : OrNode<ulong> { }

	#endregion

	#region Xor

	public abstract class XorNode<T> : BinaryOpNode<T>
	{
		protected XorNode()
			: base(OpCodes.Xor)
		{

		}
	}

	public sealed class BooleanXorNode : XorNode<bool> { }

	public sealed class Int8XorNode : XorNode<sbyte> { }
	public sealed class UInt8XorNode : XorNode<byte> { }

	public sealed class Int16XorNode : XorNode<short> { }
	public sealed class UInt16XorNode : XorNode<ushort> { }

	public sealed class Int32XorNode : XorNode<int> { }
	public sealed class UInt32XorNode : XorNode<uint> { }

	public sealed class Int64XorNode : XorNode<long> { }
	public sealed class UInt64XorNode : XorNode<ulong> { }

	#endregion

	#region Equals

	public abstract class EqualNode<T> : BinaryOpNode<T, T, bool>
	{
		protected EqualNode()
			: base(OpCodes.Ceq)
		{

		}
	}

	public sealed class Int8EqualNode : EqualNode<sbyte> { }
	public sealed class UInt8EqualNode : EqualNode<byte> { }

	public sealed class Int16EqualNode : EqualNode<short> { }
	public sealed class UInt16EqualNode : EqualNode<ushort> { }

	public sealed class Int32EqualNode : EqualNode<int> { }
	public sealed class UInt32EqualNode : EqualNode<uint> { }

	public sealed class Int64EqualNode : EqualNode<long> { }
	public sealed class UInt64EqualNode : EqualNode<ulong> { }

	public sealed class SingleEqualNode : EqualNode<float> { }
	public sealed class DoubleEqualNode : EqualNode<double> { }

	#endregion

	#region Greater Than

	public abstract class GreaterThanNode<T> : BinaryOpNode<T, T, bool>
	{
		protected GreaterThanNode()
			: base(OpCodes.Cgt)
		{

		}
	}

	public sealed class Int8GreaterThanNode : GreaterThanNode<sbyte> { }
	public sealed class UInt8GreaterThanNode : GreaterThanNode<byte> { }

	public sealed class Int16GreaterThanNode : GreaterThanNode<short> { }
	public sealed class UInt16GreaterThanNode : GreaterThanNode<ushort> { }

	public sealed class Int32GreaterThanNode : GreaterThanNode<int> { }
	public sealed class UInt32GreaterThanNode : GreaterThanNode<uint> { }

	public sealed class Int64GreaterThanNode : GreaterThanNode<long> { }
	public sealed class UInt64GreaterThanNode : GreaterThanNode<ulong> { }

	public sealed class SingleGreaterThanNode : GreaterThanNode<float> { }
	public sealed class DoubleGreaterThanNode : GreaterThanNode<double> { }

	#endregion

	#region Less Than

	public abstract class LessThanNode<T> : BinaryOpNode<T, T, bool>
	{
		protected LessThanNode()
			: base(OpCodes.Clt)
		{

		}
	}

	public sealed class Int8LessThanNode : LessThanNode<sbyte> { }
	public sealed class UInt8LessThanNode : LessThanNode<byte> { }

	public sealed class Int16LessThanNode : LessThanNode<short> { }
	public sealed class UInt16LessThanNode : LessThanNode<ushort> { }

	public sealed class Int32LessThanNode : LessThanNode<int> { }
	public sealed class UInt32LessThanNode : LessThanNode<uint> { }

	public sealed class Int64LessThanNode : LessThanNode<long> { }
	public sealed class UInt64LessThanNode : LessThanNode<ulong> { }

	public sealed class SingleLessThanNode : LessThanNode<float> { }
	public sealed class DoubleLessThanNode : LessThanNode<double> { }

	#endregion
}

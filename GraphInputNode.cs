﻿using System;
using System.Reflection.Emit;

namespace NodeGraph
{
	/// <summary>
	/// The node representation of a graph input.
	/// </summary>
	public sealed class GraphInputNode : Node
	{
		#region Fields

		private GraphInput pin;
		private Type type;
		private Input[] inputs;
		private Output[] outputs;

		#endregion

		#region Properties

		public GraphInput GraphPin
		{
			get { return this.pin; }
			internal set
			{
				this.pin = value;
				if (value != null)
					this.type = value.Type;
			}
		}

		public Type Type
		{
			get { return this.type; }
		}

		public Output Output
		{
			get { return this.outputs[0]; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new GraphInputNode.
		/// </summary>
		public GraphInputNode(Type type)
		{
			this.pin = null;
			this.type = type;
			this.Name = "Input";

			this.inputs = new Input[0];
			this.outputs = new Output[] { new Output(this, "In", this.type) };
		}

		internal GraphInputNode(GraphInput pin)
		{
			this.pin = pin;
			this.type = pin.Type;
			this.Name = pin.Name;

			this.inputs = new Input[0];
			this.outputs = new Output[] { new Output(this, "In", this.pin.Type) };
		}

		#endregion

		#region Methods

		protected override void OnNameChanged(EventArgs e)
		{
			base.OnNameChanged(e);
			if (this.pin != null)
				this.pin.Name = this.Name;
		}

		public override Input[] GetInputs()
		{
			return this.inputs;
		}

		public override Output[] GetOutputs()
		{
			return this.outputs;
		}

		public override void Emit(CodeGenerator generator)
		{
			if (generator.IO == IOMethod.Arg)
			{
				// load parameter.
				generator.IL.EmitLdarg(this.pin.Index + 1);
				generator.StoreOutput(this.Output);
			}
			else if (generator.IO == IOMethod.Dynamic)
			{
				// load array.
				generator.IL.Emit(OpCodes.Ldarg_1);
				generator.IL.EmitLdc_I4(this.pin.Index);

				// load element.
				generator.IL.Emit(OpCodes.Ldelem_Ref);
				if (this.Output.Type.IsValueType)
					generator.IL.Emit(OpCodes.Unbox_Any, this.Output.Type);
				else
					generator.IL.Emit(OpCodes.Castclass, this.Output.Type);
				generator.StoreOutput(this.Output);
			}
			else if (generator.IO == IOMethod.Embedded)
			{
				generator.LoadInput(this.pin);
				generator.StoreOutput(this.Output);
			}
		}

		#endregion
	}
}

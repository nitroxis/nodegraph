﻿using System.Reflection;

namespace NodeGraph
{
	/// <summary>
	/// Represents an output that wraps a field.
	/// </summary>
	public sealed class FieldOutput : Output
	{
		#region Fields

		private readonly FieldInfo field;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the field of the input.
		/// </summary>
		public FieldInfo Field
		{
			get { return this.field; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new FieldOutput.
		/// </summary>
		public FieldOutput(Node node, string name, FieldInfo field)
			: base(node, name, field.FieldType)
		{
			this.field = field;
		}

		#endregion

		#region Methods

		#endregion
	}
}

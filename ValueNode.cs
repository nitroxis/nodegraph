﻿using System;
using System.Reflection.Emit;

namespace NodeGraph
{
	/// <summary>
	/// Interface for nodes that have a single value of a specific type.
	/// </summary>
	public interface IValueNode
	{
		Type Type { get; }
		object Value { get; set; }
		bool AutoName { get; set; }
	}
	
	/// <summary>
	/// Represents a node that provides a simple value.
	/// </summary>
	public class ValueNode<T> : Node, IValueNode
	{
		#region Events

		public event EventHandler ValueChanged;

		#endregion

		#region Fields

		private T value;
		private readonly Output output;
		private bool autoName;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the value output pin.
		/// </summary>
		public Output Output
		{
			get { return this.output; }
		}

		/// <summary>
		/// Gets or sets the value of the node.
		/// </summary>
		[Expose]
		public T Value
		{
			get { return this.value; }
			set
			{
				if (!Equals(this.value, value))
				{
					this.value = value;
					this.OnValueChanged(EventArgs.Empty);
					if (this.autoName)
						this.setName();
				}
			}
		}

		/// <summary>
		/// Specifies whether the value is also used as name for this node.
		/// </summary>
		[Expose]
		public bool AutoName
		{
			get { return this.autoName; }
			set
			{
				this.autoName = value;
				if (value)
					this.setName();
			}
		}

		/// <summary>
		/// Gets the type.
		/// </summary>
		Type IValueNode.Type
		{
			get { return typeof(T); }
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		object IValueNode.Value
		{
			get { return this.value; }
			set { this.value = (T)value; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new ValueNode.
		/// </summary>
		public ValueNode()
			: this(default(T))
		{

		}

		/// <summary>
		/// Creates a new ValueNode using the specified value.
		/// </summary>
		public ValueNode(T value)
		{
			this.output = new Output(this, "Value", typeof(T));
			this.autoName = true; 
			this.value = value;
			this.setName();
		}

		#endregion

		#region Methods

		private void setName()
		{
			object v = this.value;
			this.Name = v == null ? "null" : this.Value.ToString();
		}

		public override Input[] GetInputs()
		{
			return new Input[0];
		}

		public override Output[] GetOutputs()
		{
			return new Output[] {this.output};
		}

		public override void Emit(CodeGenerator generator)
		{
			object v = this.Value;

			// put value on stack.
			if (typeof(T) == typeof(bool))
			{
				if ((bool)v)
					generator.IL.Emit(OpCodes.Ldc_I4_0);
				else
					generator.IL.Emit(OpCodes.Ldc_I4_1);				
			}
			else if (typeof(T) == typeof(byte))
			{
				generator.IL.EmitLdc_I4((byte)v);
			}
			else if (typeof(T) == typeof(sbyte))
			{
				generator.IL.EmitLdc_I4((sbyte)v);
			}
			else if (typeof(T) == typeof(short))
			{
				generator.IL.EmitLdc_I4((short)v);
			}
			else if (typeof(T) == typeof(ushort))
			{
				generator.IL.EmitLdc_I4((ushort)v);
			}
			else if (typeof(T) == typeof(int))
			{
				generator.IL.EmitLdc_I4((int)v);
			}
			else if (typeof(T) == typeof(uint))
			{
				generator.IL.EmitLdc_I4(unchecked((int)((uint)v)));
			}
			else if (typeof(T) == typeof(long))
			{
				generator.IL.Emit(OpCodes.Ldc_I8, (long)v);
			}
			else if (typeof(T) == typeof(ulong))
			{
				generator.IL.Emit(OpCodes.Ldc_I8, unchecked((long)((ulong)v)));
			}
			else if (typeof(T) == typeof(float))
			{
				generator.IL.Emit(OpCodes.Ldc_R4, (float)v);
			}
			else if (typeof(T) == typeof(double))
			{
				generator.IL.Emit(OpCodes.Ldc_R8, (double)v);
			}
			else if (typeof(T) == typeof(char))
			{
				generator.IL.EmitLdc_I4((char)v);
			}
			else if (typeof(T) == typeof(string))
			{
				generator.IL.Emit(OpCodes.Ldstr, (string)v);
			}
			else
			{
				int index = generator.State.AddObject(this.value);
				
				generator.IL.Emit(OpCodes.Ldarg_0);
				generator.IL.Emit(OpCodes.Ldfld, GraphState.ObjectField);
				generator.IL.EmitLdc_I4(index);
				generator.IL.Emit(OpCodes.Ldelem_Ref);

				if (typeof(T).IsValueType)
					generator.IL.Emit(OpCodes.Unbox_Any, typeof(T));
				else
					generator.IL.Emit(OpCodes.Isinst, typeof(T));
			}

			generator.StoreOutput(this.Output);
		}

		protected virtual void OnValueChanged(EventArgs e)
		{
			if (this.ValueChanged != null)
				this.ValueChanged(this, e);

			this.OnNodeChanged(EventArgs.Empty);
		}

		#endregion

	}
}

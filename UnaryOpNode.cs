﻿using System.Reflection.Emit;

namespace NodeGraph
{
	/// <summary>
	/// Performs a unary IL operation. 
	/// </summary>
	public abstract class UnaryOpNode<TIn, TOut> : Node
	{
		#region Fields

		private readonly OpCode[] instructions;
		private readonly Input input;
		private readonly Output output;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the IL instructions that are performed.
		/// </summary>
		public OpCode[] Instructions
		{
			get { return this.instructions; }
		}

		/// <summary>
		/// First operand.
		/// </summary>
		public Input Input
		{
			get { return this.input; }
		}

		/// <summary>
		/// Second operand.
		/// </summary>
		public Output Output
		{
			get { return this.output; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new AddNode.
		/// </summary>
		protected UnaryOpNode(params OpCode[] instructions)
		{
			this.instructions = instructions;
			this.input = new Input(this, "Input", typeof(TIn));
			this.output = new Output(this, "Output", typeof(TOut));
		}

		#endregion

		#region Methods

		public override Input[] GetInputs()
		{
			return new[] {this.input};
		}

		public override Output[] GetOutputs()
		{
			return new[] {this.output};
		}

		public override void Emit(CodeGenerator generator)
		{
			generator.LoadInput(this.input);
			foreach (OpCode instruction in this.instructions)
				generator.IL.Emit(instruction);
			generator.StoreOutput(this.output);
		}

		#endregion
	}

	/// <summary>
	/// Performs a unary IL operation. 
	/// </summary>
	public abstract class UnaryOpNode<T> : UnaryOpNode<T, T>
	{
		#region Constructors

		/// <summary>
		/// Creates a new AddNode.
		/// </summary>
		protected UnaryOpNode(params OpCode[] instructions)
			: base(instructions)
		{

		}

		#endregion
	}

	#region Negate

	public abstract class NegateNode<T> : UnaryOpNode<T>
	{
		protected NegateNode() : base(OpCodes.Neg) { }
	}

	public sealed class Int8NegateNode : NegateNode<sbyte> { }
	public sealed class Int16NegateNode : NegateNode<short> { }
	public sealed class Int32NegateNode : NegateNode<int> { }
	public sealed class Int64NegateNode : NegateNode<long> { }
	public sealed class SingleNegateNode : NegateNode<float> { }
	public sealed class DoubleNegateNode : NegateNode<double> { }

	#endregion

	#region Not

	public abstract class NotNode<T> : UnaryOpNode<T>
	{
		protected NotNode() : base(OpCodes.Not) { }
	}

	public class BooleanNotNode : UnaryOpNode<bool>
	{
		public BooleanNotNode() : base(OpCodes.Ldc_I4_0, OpCodes.Ceq) { }
	}

	public sealed class Int8NotNode : NotNode<sbyte> { }
	public sealed class UInt8NotNode : NotNode<byte> { }

	public sealed class Int16NotNode : NotNode<short> { }
	public sealed class UInt16NotNode : NotNode<ushort> { }

	public sealed class Int32NotNode : NotNode<int> { }
	public sealed class UInt32NotNode : NotNode<uint> { }

	public sealed class Int64NotNode : NotNode<long> { }
	public sealed class UInt64NotNode : NotNode<ulong> { }

	#endregion

	#region Conversions

	#region From Int32

	public sealed class Int32ToInt8Node : UnaryOpNode<int, sbyte>
	{
		public Int32ToInt8Node() : base(OpCodes.Conv_I1) { }
	}

	public sealed class Int32ToUInt8Node : UnaryOpNode<int, byte>
	{
		public Int32ToUInt8Node() : base(OpCodes.Conv_U1) { }
	}

	public sealed class Int32ToInt16Node : UnaryOpNode<int, short>
	{
		public Int32ToInt16Node() : base(OpCodes.Conv_I2) { }
	}

	public sealed class Int32ToUInt16Node : UnaryOpNode<int, ushort>
	{
		public Int32ToUInt16Node() : base(OpCodes.Conv_U2) { }
	}

	public sealed class Int32ToUInt32Node : UnaryOpNode<int, uint>
	{
		public Int32ToUInt32Node() : base(OpCodes.Conv_U4) { }
	}

	public sealed class Int32ToInt64Node : UnaryOpNode<int, long>
	{
		public Int32ToInt64Node() : base(OpCodes.Conv_I8) { }
	}

	public sealed class Int32ToUInt64Node : UnaryOpNode<int, ulong>
	{
		public Int32ToUInt64Node() : base(OpCodes.Conv_U8) { }
	}

	public sealed class Int32ToSingleNode : UnaryOpNode<int, float>
	{
		public Int32ToSingleNode() : base(OpCodes.Conv_R4) { }
	}

	public sealed class Int32ToDoubleNode : UnaryOpNode<int, double>
	{
		public Int32ToDoubleNode() : base(OpCodes.Conv_R8) { }
	}

	#endregion

	#region From Single

	public sealed class SingleToInt8Node : UnaryOpNode<float, sbyte>
	{
		public SingleToInt8Node() : base(OpCodes.Conv_I1) { }
	}

	public sealed class SingleToUInt8Node : UnaryOpNode<float, byte>
	{
		public SingleToUInt8Node() : base(OpCodes.Conv_U1) { }
	}

	public sealed class SingleToInt16Node : UnaryOpNode<float, short>
	{
		public SingleToInt16Node() : base(OpCodes.Conv_I2) { }
	}

	public sealed class SingleToUInt16Node : UnaryOpNode<float, ushort>
	{
		public SingleToUInt16Node() : base(OpCodes.Conv_U2) { }
	}

	public sealed class SingleToInt32Node : UnaryOpNode<float, int>
	{
		public SingleToInt32Node() : base(OpCodes.Conv_I4) { }
	}

	public sealed class SingleToUInt32Node : UnaryOpNode<float, uint>
	{
		public SingleToUInt32Node() : base(OpCodes.Conv_U4) { }
	}

	public sealed class SingleToInt64Node : UnaryOpNode<float, long>
	{
		public SingleToInt64Node() : base(OpCodes.Conv_I8) { }
	}

	public sealed class SingleToUInt64Node : UnaryOpNode<float, ulong>
	{
		public SingleToUInt64Node() : base(OpCodes.Conv_U8) { }
	}

	public sealed class SingleToDoubleNode : UnaryOpNode<float, double>
	{
		public SingleToDoubleNode() : base(OpCodes.Conv_R8) { }
	}

	#endregion

	#region From Double

	public sealed class DoubleToInt8Node : UnaryOpNode<double, sbyte>
	{
		public DoubleToInt8Node() : base(OpCodes.Conv_I1) { }
	}

	public sealed class DoubleToUInt8Node : UnaryOpNode<double, byte>
	{
		public DoubleToUInt8Node() : base(OpCodes.Conv_U1) { }
	}

	public sealed class DoubleToInt16Node : UnaryOpNode<double, short>
	{
		public DoubleToInt16Node() : base(OpCodes.Conv_I2) { }
	}

	public sealed class DoubleToUInt16Node : UnaryOpNode<double, ushort>
	{
		public DoubleToUInt16Node() : base(OpCodes.Conv_U2) { }
	}

	public sealed class DoubleToInt32Node : UnaryOpNode<double, int>
	{
		public DoubleToInt32Node() : base(OpCodes.Conv_I4) { }
	}

	public sealed class DoubleToUInt32Node : UnaryOpNode<double, uint>
	{
		public DoubleToUInt32Node() : base(OpCodes.Conv_U4) { }
	}

	public sealed class DoubleToInt64Node : UnaryOpNode<double, long>
	{
		public DoubleToInt64Node() : base(OpCodes.Conv_I8) { }
	}

	public sealed class DoubleToUInt64Node : UnaryOpNode<double, ulong>
	{
		public DoubleToUInt64Node() : base(OpCodes.Conv_U8) { }
	}

	public sealed class DoubleToSingleNode : UnaryOpNode<double, float>
	{
		public DoubleToSingleNode() : base(OpCodes.Conv_R4) { }
	}

	#endregion

	#endregion
}

﻿using System;

namespace NodeGraph
{
	/// <summary>
	/// Represents EventArgs that contain a connection.
	/// </summary>
	public sealed class ConnectionEventArgs : EventArgs
	{
		#region Fields

		private readonly Connection connection;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the connection.
		/// </summary>
		public Connection Connection
		{
			get { return this.connection; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new ConnectionEventArgs.
		/// </summary>
		public ConnectionEventArgs(Connection connection)
		{
			this.connection = connection;
		}

		#endregion

		#region Methods

		#endregion
	}
}

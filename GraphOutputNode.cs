﻿using System;
using System.Reflection.Emit;

namespace NodeGraph
{
	/// <summary>
	/// The node representation of a graph output.
	/// </summary>
	public sealed class GraphOutputNode : Node
	{
		#region Fields

		private GraphOutput pin;
		private Type type;
		private Input[] inputs;
		private Output[] outputs;

		#endregion

		#region Properties

		public GraphOutput GraphPin
		{
			get { return this.pin; }
			internal set
			{
				this.pin = value;
				if (value != null)
					this.type = value.Type;
			}
		}

		public Type Type
		{
			get { return this.type; }
		}

		public Input Input
		{
			get { return this.inputs[0]; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new GraphOutputNode.
		/// </summary>
		public GraphOutputNode(Type type)
		{
			this.pin = null;
			this.type = type;
			this.Name = "Output";

			this.inputs = new Input[] { new Input(this, "Out", this.type) };
			this.outputs = new Output[0];
		}

		internal GraphOutputNode(GraphOutput pin)
		{
			this.pin = pin;
			this.Name = pin.Name;
			this.type = pin.Type;
			
			this.inputs = new Input[] { new Input(this, "Out", this.pin.Type) };
			this.outputs = new Output[0];
		}

		#endregion

		#region Methods

		protected override void OnNameChanged(EventArgs e)
		{
			base.OnNameChanged(e);
			if (this.pin != null)
				this.pin.Name = this.Name;
		}

		public override Input[] GetInputs()
		{
			return this.inputs;
		}

		public override Output[] GetOutputs()
		{
			return this.outputs;
		}

		public override void Emit(CodeGenerator generator)
		{
			if (generator.IO == IOMethod.Arg)
			{
				// load out parameter.
				generator.IL.EmitLdarg(this.Graph.GetInputs().Length + this.GraphPin.Index + 1);

				// load input.
				generator.LoadInput(this.Input);

				// store input in parameter.
				Type type = this.GraphPin.Type;
				if (type == typeof(bool) || type == typeof(byte) || type == typeof(sbyte))
					generator.IL.Emit(OpCodes.Stind_I1);
				else if (type == typeof(short) || type == typeof(ushort) || type == typeof(char))
					generator.IL.Emit(OpCodes.Stind_I2);
				else if (type == typeof(int) || type == typeof(uint))
					generator.IL.Emit(OpCodes.Stind_I4);
				else if (type == typeof(long) || type == typeof(ulong))
					generator.IL.Emit(OpCodes.Stind_I8);
				else if (type == typeof(float))
					generator.IL.Emit(OpCodes.Stind_R4);
				else if (type == typeof(double))
					generator.IL.Emit(OpCodes.Stind_R8);
				else if (type.IsSubclassOf(typeof(object)))
					generator.IL.Emit(OpCodes.Stind_Ref);
			}
			else if (generator.IO == IOMethod.Dynamic)
			{
				// load array.
				generator.IL.Emit(OpCodes.Ldarg_2);
				generator.IL.EmitLdc_I4(this.GraphPin.Index);

				// load and box value.
				generator.LoadInput(this.Input);
				generator.IL.Emit(OpCodes.Box, this.Input.Type);

				// store value.
				generator.IL.Emit(OpCodes.Stelem_Ref);
			}
			else if (generator.IO == IOMethod.Embedded)
			{
				generator.LoadInput(this.Input);
				generator.StoreOutput(this.pin);
			}
		}

		#endregion
	}
}

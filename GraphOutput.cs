﻿using System;

namespace NodeGraph
{
	/// <summary>
	/// Represents an output of a graph.
	/// </summary>
	public sealed class GraphOutput : Output
	{
		#region Fields

		private readonly GraphOutputNode pinNode;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the node of the output.
		/// </summary>
		public GraphOutputNode PinNode
		{
			get { return this.pinNode; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new GraphOutput.
		/// </summary>
		internal GraphOutput(Graph graph, string name, Type type)
			: base(graph, name, type)
		{
			this.pinNode = new GraphOutputNode(this);
		}

		/// <summary>
		/// Creates a new GraphOutput.
		/// </summary>
		internal GraphOutput(Graph graph, GraphOutputNode node)
			: base(graph, node.Name, node.Type)
		{
			this.pinNode = node;
		}

		#endregion

		#region Methods

		#endregion
	}
}

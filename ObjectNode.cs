﻿namespace NodeGraph
{
	/// <summary>
	/// Represents a node as object with a process method and internal state.
	/// </summary>
	public abstract class ObjectNode : Node
	{
		#region Fields

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new ObjectNode.
		/// </summary>
		protected ObjectNode()
		{

		}

		#endregion

		#region Methods

		/// <summary>
		/// Processes the node.
		/// </summary>
		public abstract void Process();

		public override Input[] GetInputs()
		{
			throw new System.NotImplementedException();
		}

		public override Output[] GetOutputs()
		{
			throw new System.NotImplementedException();
		}

		#endregion
	}
}

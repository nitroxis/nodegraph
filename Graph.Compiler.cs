﻿using System;
using System.Reflection.Emit;

namespace NodeGraph
{
	public delegate void DynamicGraph(object[] inputs, object[] outputs);

	/// <summary>
	/// Represents a compiler for node graphs.
	/// </summary>
	public partial class Graph
	{
		#region Fields

		#endregion

		#region Properties

		#endregion

		#region Constructors

		#endregion

		#region Methods

		/// <summary>
		/// Compiles the node graph and returns a delegate.
		/// </summary>
		/// <returns></returns>
		public DynamicGraph Compile()
		{
			GraphState dummy;
			return this.Compile(out dummy);
		}

		/// <summary>
		/// Compiles the node graph and returns a delegate.
		/// </summary>
		/// <param name="state">The internal state.</param>
		/// <returns></returns>
		public DynamicGraph Compile(out GraphState state)
		{
			DynamicMethod method = new DynamicMethod(this.Name, typeof(void), new Type[] { typeof(GraphState), typeof(object[]), typeof(object[]) });
			ILGenerator il = method.GetILGenerator();
			state = this.compile(new CodeGenerator(il, IOMethod.Dynamic));

			return (DynamicGraph)method.CreateDelegate(typeof(DynamicGraph), state);
		}

		/// <summary>
		/// Compiles the node graph and returns a delegate.
		/// </summary>
		/// <param name="delegateType">The delegate type.</param>
		/// <returns></returns>
		public Delegate Compile(Type delegateType)
		{
			GraphState dummy;
			return this.Compile(delegateType, out dummy);
		}

		/// <summary>
		/// Compiles the node graph and returns a delegate.
		/// </summary>
		/// <param name="delegateType">The delegate type.</param>
		/// <param name="state">The internal state.</param>
		/// <returns></returns>
		public Delegate Compile(Type delegateType, out GraphState state)
		{
			Type[] parameters = new Type[1 + this.inputs.Count + this.outputs.Count];

			parameters[0] = typeof(GraphState);

			for (int i = 0; i < this.inputs.Count; i++)
				parameters[1 + i] = this.inputs[i].Type;
			
			for (int i = 0; i < this.outputs.Count; i++)
				parameters[1 + this.inputs.Count + i] = this.outputs[i].Type.MakeByRefType();
			
			DynamicMethod method = new DynamicMethod(this.Name, typeof(void), parameters);
			ILGenerator il = method.GetILGenerator();
			state = this.compile(new CodeGenerator(il, IOMethod.Arg));
			
			return method.CreateDelegate(delegateType, state);
		}
		
		private GraphState compile(CodeGenerator generator)
		{
			this.emit(generator);
			generator.IL.Emit(OpCodes.Ret);
			generator.State.Optimize();

			
			//byte[] code = generator.IL.GetType().GetMethod("BakeByteArray", BindingFlags.NonPublic | BindingFlags.Instance).Invoke(generator.IL, null) as byte[]; // DIRTIEST HACK EVER MADE
			//Console.WriteLine(Helper.DumpIL(code, null));
			//Console.WriteLine("Cyclic connections: {0}", this.HasCyclicConnection() ? "Yes" : "No");
			//Console.WriteLine("Code Generator: {0}", generator.GetType().Name);
			//Console.WriteLine("IL Size: {0}", code.Length);
			//Console.WriteLine("Internal state:");
			//Console.WriteLine(" - Boolean: {0}", generator.State.Boolean.Length);
			//Console.WriteLine(" - Int8: {0}", generator.State.Int8.Length);
			//Console.WriteLine(" - UInt8: {0}", generator.State.UInt8.Length);
			//Console.WriteLine(" - Int16: {0}", generator.State.Int16.Length);
			//Console.WriteLine(" - UInt16: {0}", generator.State.UInt16.Length);
			//Console.WriteLine(" - Int32: {0}", generator.State.Int32.Length);
			//Console.WriteLine(" - UInt32: {0}", generator.State.UInt32.Length);
			//Console.WriteLine(" - Int64: {0}", generator.State.Int64.Length);
			//Console.WriteLine(" - UInt64: {0}", generator.State.UInt64.Length);
			//Console.WriteLine(" - Single: {0}", generator.State.Single.Length);
			//Console.WriteLine(" - Double: {0}", generator.State.Double.Length);
			//Console.WriteLine(" - Char: {0}", generator.State.Char.Length);
			//Console.WriteLine(" - String: {0}", generator.State.String.Length);
			//Console.WriteLine(" - Object: {0}", generator.State.Object.Length);
			//Console.WriteLine("TOTAL: {0}", generator.State.Boolean.Length + generator.State.Int8.Length + generator.State.UInt8.Length + generator.State.Int16.Length + generator.State.UInt16.Length + generator.State.Int32.Length + generator.State.UInt32.Length + generator.State.Int64.Length + generator.State.UInt64.Length + generator.State.Single.Length + generator.State.Double.Length + generator.State.Char.Length + generator.State.String.Length + generator.State.Object.Length);
			

			return generator.State;
		}
		
		public override void Emit(CodeGenerator generator)
		{
			IOMethod io = generator.IO;
			generator.IO = IOMethod.Embedded;
			this.emit(generator);
			generator.IO = io;
		}

		private void emit(CodeGenerator generator)
		{
			foreach (GraphOutput output in this.outputs)
				this.emit(generator, output.PinNode);
			foreach (Node node in this.nodes)
				this.emit(generator, node);
		}

		private void emit(CodeGenerator context, Node node)
		{
			if (context.ProcessedNodes.Contains(node))
				return;

			context.ProcessedNodes.Add(node);

			Input[] inputs = node.GetInputs();
			Output[] outputs = node.GetOutputs();

			for (int i = 0; i < outputs.Length; i++)
				context.CreateOutputStorage(outputs[i]);

			for (int i = 0; i < inputs.Length; i++)
				this.emitInput(context, inputs[i]);
			
			node.Emit(context);
		}

		private void emitInput(CodeGenerator context, Input input)
		{
			Output source = input.GetSource();
			if (source == null)
				return;

			Node sourceNode = source.Node;
			if (context.ProcessedNodes.Contains(sourceNode))
				return;

			this.emit(context, sourceNode);
		}

		#endregion
	}
}

﻿using System;

namespace NodeGraph
{
	/// <summary>
	/// Specifies that a property should be exposed to external editors.
	/// </summary>
	[AttributeUsage(AttributeTargets.Property, AllowMultiple=false)]
	public class ExposeAttribute : Attribute
	{
		#region Constructors

		/// <summary>
		/// Creates a new NodeDataAttribute.
		/// </summary>
		public ExposeAttribute()
		{

		}

		#endregion

	}
}

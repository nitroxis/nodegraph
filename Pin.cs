﻿using System;

namespace NodeGraph
{
	/// <summary>
	/// Represents an abstract pin.
	/// </summary>
	public abstract class Pin
	{
		#region Events

		public event EventHandler NameChanged;

		#endregion

		#region Fields

		private readonly Node node;
		private string name;
		private readonly Type type;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the node that defines this pin.
		/// </summary>
		public Node Node
		{
			get { return this.node; }
		}

		/// <summary>
		/// Gets the graph that owns the node of this pin.
		/// </summary>
		public Graph Graph
		{
			get { return this.node.Graph; }
		}

		/// <summary>
		/// Gets the name of the pin.
		/// </summary>
		public string Name
		{
			get { return this.name; }
			set
			{
				if (value != this.name)
				{
					this.name = value;
					this.OnNameChanged(EventArgs.Empty);
				}
			}
		}

		/// <summary>
		/// Gets the type of the pin.
		/// </summary>
		public Type Type
		{
			get { return this.type; }
		}

		/// <summary>
		/// Gets a value that indicates whether this pin is lazy.
		/// </summary>
		public bool IsLazy
		{
			get { return this.Type.ImplementsInterface(typeof(ILazy)); }
		}

		/// <summary>
		/// Gets the index of the pin.
		/// </summary>
		/// <returns></returns>
		public abstract int Index { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new pin.
		/// </summary>
		protected Pin(Node node, string name, Type type)
		{
			this.node = node;
			this.name = name;
			this.type = type;
		}

		#endregion

		#region Methods

		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}

		public override int GetHashCode()
		{
			return this.Name.GetHashCode() ^ this.Node.GetHashCode() ^ this.Type.GetHashCode();
		}

		public override string ToString()
		{
			return this.Name;
		}

		protected virtual void OnNameChanged(EventArgs e)
		{
			if (this.NameChanged != null)
				this.NameChanged(this, e);

			this.Node.RaisePinsChanged();
		}

		#endregion

		#region Operators

		public static bool operator ==(Pin a, Pin b)
		{
			return object.Equals(a, b);
		}

		public static bool operator !=(Pin a, Pin b)
		{
			return !object.Equals(a, b);
		}

		#endregion
	}
}

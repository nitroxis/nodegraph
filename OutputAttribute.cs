﻿using System;

namespace NodeGraph
{
	/// <summary>
	/// Marks a field or property as output of a class node.
	/// </summary>
	[AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
	public class OutputAttribute : PinAttribute
	{
		#region Fields

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new OutputAttribute.
		/// </summary>
		public OutputAttribute()
		{

		}

		#endregion

		#region Methods

		#endregion
	}
}

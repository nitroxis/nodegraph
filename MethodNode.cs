﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;

namespace NodeGraph
{
	/// <summary>
	/// Represents a node that wraps a method.
	/// </summary>
	public sealed class MethodNode : Node
	{
		#region Fields

		private readonly object target;
		private readonly MethodInfo method;
		private readonly ParameterInfo[] parameters;
		private readonly Input[] inputs;
		private readonly Output[] outputs;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the underlying method.
		/// </summary>
		public MethodInfo Method
		{
			get { return this.method; }
		}

		#endregion

		#region Constructors


		/// <summary>
		/// Creates a new MethodNode.
		/// </summary>
		public MethodNode(MethodInfo method)
			: this(method, null)
		{
			
		}

		/// <summary>
		/// Creates a new MethodNode.
		/// </summary>
		public MethodNode(MethodInfo method, object target)
		{
			if (method == null)
				throw new ArgumentNullException("method");
			if (!method.IsStatic && target == null)
				throw new ArgumentNullException("target");

			this.method = method;
			this.target = target;

			List<Input> inParams = new List<Input>();
			List<Output> outParams = new List<Output>();
			this.parameters = method.GetParameters();

			if (method.ReturnType != typeof(void))
				outParams.Add(new Output(this, "Result", method.ReturnType));

			foreach (ParameterInfo parameter in this.parameters)
			{
				Type type = parameter.ParameterType;
				if (type.IsByRef)
				{
					outParams.Add(new Output(this, parameter.Name, type.GetElementType()));
				}
				else
				{
					inParams.Add(new Input(this, parameter.Name, type));
				}
			}

			this.inputs = inParams.ToArray();
			this.outputs = outParams.ToArray();
		}

		#endregion

		#region Methods

		public override Input[] GetInputs()
		{
			return this.inputs;
		}

		public override Output[] GetOutputs()
		{
			return this.outputs;
		}

		public override void Emit(CodeGenerator generator)
		{
			List<LocalBuilder> outputLocals = new List<LocalBuilder>();

			// <out> locals
			for (int i = this.parameters.Length - 1; i >= 0; i--)
			{
				if (this.parameters[i].ParameterType.IsByRef)
				{
					LocalBuilder local = generator.TakeTempLocal(this.parameters[i].ParameterType.GetElementType());
					outputLocals.Add(local);
				}
			}
			outputLocals.Reverse();

			if (!method.IsStatic)
			{
				// load instance
				Type targetType = this.target.GetType();
				int index = Array.IndexOf(generator.State.Object, this.target);
				if (index < 0)
					index = generator.State.AddObject(this.target);

				generator.IL.Emit(OpCodes.Ldarg_0);
				generator.IL.Emit(OpCodes.Ldfld, GraphState.ObjectField);
				generator.IL.EmitLdc_I4(index);
				generator.IL.Emit(OpCodes.Ldelem_Ref);

				if (targetType.IsValueType)
					generator.IL.Emit(OpCodes.Unbox_Any, targetType);
				else
					generator.IL.Emit(OpCodes.Castclass, targetType);
			}

			int outIndex = 0;
			int inIndex = 0;

			// load inputs and load <out> parameter addresses.
			for (int i = 0; i < this.parameters.Length; i++)
			{
				if (this.parameters[i].ParameterType.IsByRef)
					generator.IL.EmitLdloca(outputLocals[outIndex++].LocalIndex);
				else
					generator.LoadInput(this.inputs[inIndex++]);
			}

			// emit call
			generator.IL.EmitCall(this.method);

			// store values of <out> locals. 
			outIndex = 0;
			if (this.method.ReturnType != typeof(void))
				generator.StoreOutput(this.outputs[outIndex++]);

			for (int i = 0; i < this.parameters.Length; i++)
			{
				if (this.parameters[i].ParameterType.IsByRef)
				{
					generator.IL.EmitLdloc(outputLocals[outIndex].LocalIndex);
					generator.StoreOutput(this.outputs[outIndex++]);
				}
			}

			foreach (LocalBuilder temp in outputLocals)
				generator.GiveTempLocal(temp);
		}

		#endregion
	}
}

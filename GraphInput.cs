﻿using System;

namespace NodeGraph
{
	/// <summary>
	/// Represents an input of a graph.
	/// </summary>
	public sealed class GraphInput : Input
	{
		#region Fields

		private readonly GraphInputNode pinNode;

		#endregion

		#region Properties

		public GraphInputNode PinNode
		{
			get { return this.pinNode; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new GraphInput.
		/// </summary>
		internal GraphInput(Graph graph, string name, Type type)
			: base(graph, name, type)
		{
			this.pinNode = new GraphInputNode(this);
		}

		/// <summary>
		/// Creates a new GraphInput.
		/// </summary>
		internal GraphInput(Graph graph, GraphInputNode node)
			: base(graph, node.Name, node.Type)
		{
			this.pinNode = node;
		}

		#endregion

		#region Methods

		#endregion
	}
}

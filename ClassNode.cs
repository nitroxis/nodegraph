﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;

namespace NodeGraph
{
	/// <summary>
	/// Wraps a type as node using fields as inputs and outputs.
	/// </summary>
	public sealed class ClassNode : Node
	{
		#region Fields

		private readonly Type type;
		private readonly ConstructorInfo constructor;
		private readonly MethodInfo processMethod;
		private readonly Input[] inputs;
		private readonly Output[] outputs;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new ClassNode.
		/// </summary>
		public ClassNode(Type type)
		{
			if (type == null)
				throw new ArgumentNullException("type");

			this.constructor = type.GetConstructor(BindingFlags.Public | BindingFlags.Instance, null, new Type[0], null);
			if (this.constructor == null)
				throw new Exception(string.Format("{0} does not have a publicly accessible parameterless constructor.", type));

			this.processMethod = type.GetMethod("Process", BindingFlags.Public | BindingFlags.Instance, null, new Type[0], null);
			if(this.processMethod == null)
				throw new Exception(string.Format("{0} does not have a publicly accessible parameterless Process() method.", type));

			this.type = type;

			List<Input> inputs = new List<Input>();
			List<Output> outputs = new List<Output>();
			FieldInfo[] fields = type.GetFields(BindingFlags.Instance | BindingFlags.Public);

			foreach (FieldInfo field in fields)
			{
				InputAttribute[] inputAttr = (InputAttribute[])field.GetCustomAttributes(typeof(InputAttribute), true);
				OutputAttribute[] outputAttr = (OutputAttribute[])field.GetCustomAttributes(typeof(OutputAttribute), true);

				if (inputAttr.Length > 0)
					inputs.Add(new FieldInput(this, inputAttr[0].Name ?? field.Name, field));

				if (outputAttr.Length > 0)
					outputs.Add(new FieldOutput(this, outputAttr[0].Name ?? field.Name, field));
			}

			this.inputs = inputs.ToArray();
			this.outputs = outputs.ToArray();
		}

		#endregion

		#region Methods

		public override Input[] GetInputs()
		{
			return this.inputs;
		}

		public override Output[] GetOutputs()
		{
			return this.outputs;
		}

		public override void Emit(CodeGenerator generator)
		{
			int instanceIndex = generator.State.AddObject(null);
			
			// load instance.
			generator.IL.Emit(OpCodes.Ldarg_0);
			generator.IL.Emit(OpCodes.Ldfld, GraphState.ObjectField);
			generator.IL.EmitLdc_I4(instanceIndex);
			generator.IL.Emit(OpCodes.Ldelem_Ref);
			generator.IL.Emit(OpCodes.Castclass, type);

			// check if instance is null.
			Label label = generator.IL.DefineLabel();
			generator.IL.Emit(OpCodes.Dup);
			generator.IL.Emit(OpCodes.Ldnull);
			generator.IL.Emit(OpCodes.Ceq);
			generator.IL.Emit(OpCodes.Brfalse, label); // branch to input loading if instance != null.

			// create new instance.
			LocalBuilder instanceTemp = generator.TakeTempLocal(this.type);
			generator.IL.Emit(OpCodes.Pop); // pop null.
			generator.IL.Emit(OpCodes.Newobj, this.constructor); // call constructor.
			generator.IL.Emit(OpCodes.Dup);
			generator.IL.EmitStloc(instanceTemp.LocalIndex); // temp store.

			// save to state.
			generator.IL.Emit(OpCodes.Ldarg_0);
			generator.IL.Emit(OpCodes.Ldfld, GraphState.ObjectField);
			generator.IL.EmitLdc_I4(instanceIndex);
			generator.IL.EmitLdloc(instanceTemp.LocalIndex); // temp load.
			generator.IL.Emit(OpCodes.Stelem_Ref); // store in state.
			generator.GiveTempLocal(instanceTemp);
			
			generator.IL.MarkLabel(label); // branch target if instance != null.

			// store inputs in fields.
			for (int i = 0; i < this.inputs.Length; i++)
			{
				generator.IL.Emit(OpCodes.Dup);
				generator.LoadInput(this.inputs[i]);

				FieldInput fieldInput = this.inputs[i] as FieldInput;
				if (fieldInput != null)
				{
					generator.IL.Emit(OpCodes.Stfld, fieldInput.Field);
				}
				else
				{
					throw new Exception("Invalid class node input.");
				}
			}

			// call Process()
			generator.IL.Emit(OpCodes.Dup);
			generator.IL.EmitCall(this.processMethod);
			
			// store fields in outputs.
			for (int i = 0; i < this.outputs.Length; i++)
			{
				generator.IL.Emit(OpCodes.Dup);

				FieldOutput fieldOutput = this.outputs[i] as FieldOutput;
				if (fieldOutput != null)
				{
					generator.IL.Emit(OpCodes.Ldfld, fieldOutput.Field);
					generator.StoreOutput(this.outputs[i]);
				}
				else
				{
					throw new Exception("Invalid class node output.");
				}
			}
			
			// done.
			generator.IL.Emit(OpCodes.Pop);
		}

		#endregion
	}
}


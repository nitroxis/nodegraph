﻿using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace NodeGraph
{
	/// <summary>
	/// Reflection and IL extensions.
	/// </summary>
	public static class Extensions
	{
		#region Methods

		/// <summary>
		/// Checks wether the specified type is implementing an interface.
		/// </summary>
		/// <param name="type">The target type.</param>
		/// <param name="interfaceType">The type of the interface.</param>
		/// <returns></returns>
		public static bool ImplementsInterface(this Type type, Type interfaceType)
		{
			return type.GetInterfaces().Any(i => i == interfaceType);
		}

		/// <summary>
		/// Checks wether the specified types are compatible (e.g. derived or the same).
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static bool IsCompatibleTo(this Type a, Type b)
		{
			if (a == b)
				return true;
			if (b.IsInterface && a.ImplementsInterface(b))
				return true;
			return a.IsSubclassOf(b);
		}

		/// <summary>
		/// Checks whether the specified type is a subclass of the raw generic type.
		/// </summary>
		/// <param name="toCheck"></param>
		/// <param name="generic"></param>
		/// <returns></returns>
		public static bool IsSubclassOfRawGeneric(this Type toCheck, Type generic)
		{
			//From http://stackoverflow.com/questions/457676/check-if-a-class-is-derived-from-a-generic-class
			while (toCheck != null && toCheck != typeof(object))
			{
				var cur = toCheck.IsGenericType ? toCheck.GetGenericTypeDefinition() : toCheck;
				if (generic == cur)
					return true;
				toCheck = toCheck.BaseType;
			}
			return false;
		}

		/// <summary>
		/// Emits the shortest form of Ldc_I4 for the specified value.
		/// </summary>
		/// <param name="generator"></param>
		/// <param name="value"></param>
		public static void EmitLdc_I4(this ILGenerator generator, int value)
		{
			if (value == 0)
				generator.Emit(OpCodes.Ldc_I4_0);
			else if (value == 1)
				generator.Emit(OpCodes.Ldc_I4_1);
			else if (value == 2)
				generator.Emit(OpCodes.Ldc_I4_2);
			else if (value == 3)
				generator.Emit(OpCodes.Ldc_I4_3);
			else if (value == 4)
				generator.Emit(OpCodes.Ldc_I4_4);
			else if (value == 5)
				generator.Emit(OpCodes.Ldc_I4_5);
			else if (value == 6)
				generator.Emit(OpCodes.Ldc_I4_6);
			else if (value == 7)
				generator.Emit(OpCodes.Ldc_I4_7);
			else if (value == 8)
				generator.Emit(OpCodes.Ldc_I4_8);
			else if (value == -1)
				generator.Emit(OpCodes.Ldc_I4_M1);
			else if (value >= 0 && value <= 255)
				generator.Emit(OpCodes.Ldc_I4_S, (byte)value);
			else
				generator.Emit(OpCodes.Ldc_I4, value);
		}

		/// <summary>
		/// Emits the shortest form of Ldloc for the specified index.
		/// </summary>
		/// <param name="generator"></param>
		/// <param name="index"></param>
		public static void EmitLdloc(this ILGenerator generator, int index)
		{
			if (index == 0)
				generator.Emit(OpCodes.Ldloc_0);
			else if (index == 1)
				generator.Emit(OpCodes.Ldloc_1);
			else if (index == 2)
				generator.Emit(OpCodes.Ldloc_2);
			else if (index == 3)
				generator.Emit(OpCodes.Ldloc_3);
			else if (index < 255)
				generator.Emit(OpCodes.Ldloc_S, (byte)index);
			else
				generator.Emit(OpCodes.Ldloc, index);
		}

		/// <summary>
		/// Emits the shortest form of Stloc for the specified index.
		/// </summary>
		/// <param name="generator"></param>
		/// <param name="index"></param>
		public static void EmitStloc(this ILGenerator generator, int index)
		{
			if (index == 0)
				generator.Emit(OpCodes.Stloc_0);
			else if (index == 1)
				generator.Emit(OpCodes.Stloc_1);
			else if (index == 2)
				generator.Emit(OpCodes.Stloc_2);
			else if (index == 3)
				generator.Emit(OpCodes.Stloc_3);
			else if (index < 255)
				generator.Emit(OpCodes.Stloc_S, (byte)index);
			else
				generator.Emit(OpCodes.Stloc, index);
		}

		/// <summary>
		/// Emits the shortest form of Ldarg for the specified index.
		/// </summary>
		/// <param name="generator"></param>
		/// <param name="index"></param>
		public static void EmitLdarg(this ILGenerator generator, int index)
		{
			if (index == 0)
				generator.Emit(OpCodes.Ldarg_0);
			else if (index == 1)
				generator.Emit(OpCodes.Ldarg_1);
			else if (index == 2)
				generator.Emit(OpCodes.Ldarg_2);
			else if (index == 3)
				generator.Emit(OpCodes.Ldarg_3);
			else if (index <= 255)
				generator.Emit(OpCodes.Ldarg_S, (byte)index);
			else
				generator.Emit(OpCodes.Ldarg, index);
		}

		/// <summary>
		/// Emits the shortest form of Ldloca for the specified index.
		/// </summary>
		/// <param name="generator"></param>
		/// <param name="index"></param>
		public static void EmitLdloca(this ILGenerator generator, int index)
		{
			if (index <= 255)
				generator.Emit(OpCodes.Ldloca_S, (byte)index);
			else
				generator.Emit(OpCodes.Ldloca, index);
		}

		/// <summary>
		/// Emits a call to the speciied method.
		/// </summary>
		/// <param name="generator"></param>
		/// <param name="method"></param>
		public static void EmitCall(this ILGenerator generator, MethodBase method)
		{
			if (method.IsStatic || method.DeclaringType.IsValueType)
				generator.EmitCallStatic(method);
			else
				generator.EmitCallVirtual(method);
		}

		/// <summary>
		/// Emits a call to a static method.
		/// </summary>
		/// <param name="generator"></param>
		/// <param name="method"></param>
		public static void EmitCallStatic(this ILGenerator generator, MethodBase method)
		{
			if (method is ConstructorInfo)
				generator.Emit(OpCodes.Call, (ConstructorInfo)method);
			else if (method is MethodInfo)
				generator.Emit(OpCodes.Call, (MethodInfo)method);
			else
				throw new InvalidOperationException("Unsupported subtype of MethodBase.");
		}

		/// <summary>
		/// Emits a call to a virtual method.
		/// </summary>
		/// <param name="generator"></param>
		/// <param name="method"></param>
		public static void EmitCallVirtual(this ILGenerator generator, MethodBase method)
		{
			if (method == null)
				throw new ArgumentNullException("method");

			if (method.IsStatic)
				throw new ArgumentException("Static methods cannot be called this method.", "method");

			if (method is ConstructorInfo)
				generator.Emit(OpCodes.Callvirt, (ConstructorInfo)method);
			else if (method is MethodInfo)
				generator.Emit(OpCodes.Callvirt, (MethodInfo)method);
			else
				throw new InvalidOperationException("Unsupported subtype of MethodBase.");
		}

		#endregion
	}
}

﻿using System;
using System.IO;

namespace NodeGraph
{
	/// <summary>
	/// Represents a connection between an output and an input.
	/// </summary>
	public sealed class Connection
	{
		#region Fields

		private readonly Graph graph;
		private readonly Output source;
		private readonly Input destination;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the graph that owns the pins/nodes.
		/// </summary>
		public Graph Graph
		{
			get { return this.graph; }
		}

		/// <summary>
		/// Gets the source node.
		/// </summary>
		public Node SourceNode
		{
			get { return this.source.Node; }
		}

		/// <summary>
		/// Gets the source pin.
		/// </summary>
		public Output Source
		{
			get { return this.source; }
		}

		/// <summary>
		/// Gets the destination node.
		/// </summary>
		public Node DestinationNode
		{
			get { return this.destination.Node; }
		}

		/// <summary>
		/// Gets the destination pin.
		/// </summary>
		public Input Destination
		{
			get { return this.destination; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Connection.
		/// </summary>
		public Connection(Output source, Input destination)
		{
			this.graph = source.Graph;
			if (destination.Graph != this.graph)
				throw new Exception("Graphs do not match.");

			if (!Connection.ValidateConnection(source, destination))
				throw new Exception("Invalid connection.");

			this.source = source;
			this.destination = destination;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Removes the connection.
		/// </summary>
		public void Disconnect()
		{
			this.destination.Disconnect();
		}

		public override string ToString()
		{
			return string.Format("{0}.{1} -> {2}.{3}", this.SourceNode.Name, this.Source.Name, this.DestinationNode.Name, this.Destination.Name);
		}

		/// <summary>
		/// Checks if a connection between two pins is theoretically possible (i.e. compatible types).
		/// </summary>
		/// <param name="source"></param>
		/// <param name="destination"></param>
		/// <returns></returns>
		public static bool ValidateConnection(Output source, Input destination)
		{
			if (source == null || destination == null)
				return false;

			if (!source.Type.IsCompatibleTo(destination.Type) && source.Type != typeof(object))
			{
				if (destination.Type.IsSubclassOfRawGeneric(typeof(Lazy<>)))
				{
					Type genType = destination.Type.GetGenericArguments()[0];
					if (!source.Type.IsCompatibleTo(genType))
						return false;
				}
				else
				{
					return false;
				}
			}
			return true;
		}

		#endregion
	}
}

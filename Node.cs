﻿using System;
using System.Collections;

namespace NodeGraph
{
	/// <summary>
	/// Provides information about a simulation processor node.
	/// </summary>
	public abstract class Node
	{
		#region Events

		/// <summary>
		/// Fired when the name of the node has changed.
		/// </summary>
		public event EventHandler NameChanged;
		/// <summary>
		/// Fired when the pin configuration of the node has changed.
		/// </summary>
		public event EventHandler PinsChanged;
		/// <summary>
		/// Fired when anything in the node has changed.
		/// </summary>
		public event EventHandler NodeChanged;
		/// <summary>
		/// Fired when the parent graph has changed.
		/// </summary>
		public event EventHandler GraphChanged;

		#endregion

		#region Fields

		private Graph graph;
		private string name;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the name of the node.
		/// </summary>
		[Expose]
		public string Name
		{
			get { return this.name; }
			set
			{
				this.name = value;
				this.OnNameChanged(EventArgs.Empty);
			}
		}

		/// <summary>
		/// Gets the graph that owns the node.
		/// </summary>
		public Graph Graph
		{
			get { return this.graph; }
			internal set
			{
				this.graph = value;
				this.OnGraphChanged(EventArgs.Empty);
			}
		}

		/// <summary>
		/// Gets the index of the node in the node graph.
		/// </summary>
		public int Index
		{
			get
			{
				if (this.graph == null) return 0;
			   
				return this.graph.GetNodeIndex(this);
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Node.
		/// </summary>
		protected Node()
		{
			this.name = "Node";
		}

		#endregion

		#region Methods

		/// <summary>
		/// Gets the inputs of the node.
		/// </summary>
		/// <returns></returns>
		public abstract Input[] GetInputs();

		/// <summary>
		/// Gets the input with the specified index.
		/// </summary>
		/// <param name="index"></param>
		/// <returns></returns>
		public Input GetInput(int index)
		{
			return this.GetInputs()[index];
		}

		/// <summary>
		/// Gets the outputs of the node.
		/// </summary>
		/// <returns></returns>
		public abstract Output[] GetOutputs();

		/// <summary>
		/// Gets the output with the specified index.
		/// </summary>
		/// <param name="index"></param>
		/// <returns></returns>
		public Output GetOutput(int index)
		{
			return this.GetOutputs()[index];
		}

		/// <summary>
		/// Emits code for this node to the specified code generator.
		/// </summary>
		/// <param name="generator"></param>
		public abstract void Emit(CodeGenerator generator);

		/// <summary>
		/// Gets all pins (inputs and outputs) of the node.
		/// </summary>
		/// <returns></returns>
		public Pin[] GetPins()
		{
			Input[] inputs = this.GetInputs();
			Output[] outputs = this.GetOutputs();
			Pin[] pins = new Pin[outputs.Length + inputs.Length];
			inputs.CopyTo(pins, 0);
			outputs.CopyTo(pins, inputs.Length);
			return pins;
		}

		/// <summary>
		/// Returns the connection ends in the specified input.
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		public Connection GetSource(Input input)
		{
			return this.graph.GetSource(this, input);
		}

		/// <summary>
		/// Returns the connections that start at the specified output.
		/// </summary>
		/// <param name="output"></param>
		/// <returns></returns>
		public Connection[] GetDestinations(Output output)
		{
			return this.graph.GetDestinations(this, output);
		}

		/// <summary>
		/// Returns all connections that end in this node.
		/// </summary>
		/// <returns></returns>
		public Connection[] GetIncomingConnections()
		{
			return this.graph.GetIncomingConnections(this);
		}

		/// <summary>
		/// Returns all connections that either start or end in this node.
		/// </summary>
		/// <returns></returns>
		public Connection[] GetAllConnections()
		{
			return this.graph.GetAllConnections(this);
		}

		/// <summary>
		/// Returns the node instances that the this node depends on.
		/// </summary>
		/// <returns></returns>
		public Node[] GetDependencies()
		{
			return this.graph.GetDependencies(this);
		}

		/// <summary>
		/// Returns all connections that start in this node.
		/// </summary>
		/// <returns></returns>
		public Connection[] GetOutgoingConnections()
		{
			return this.graph.GetOutgoingConnections(this);
		}

		/// <summary>
		/// Disconnects all incoming and outgoing connections of the node.
		/// </summary>
		public void DisconnectAll()
		{
			Connection[] connections = this.GetAllConnections();
			foreach (Connection connection in connections)
				this.graph.Disconnect(connection);
		}

		/// <summary>
		/// Removes this node from the simulation.
		/// </summary>
		public void Remove()
		{
			this.graph.RemoveNode(this);
		}

		public override string ToString()
		{
			return this.Name;
		}

		internal void RaisePinsChanged()
		{
			this.OnPinsChanged(EventArgs.Empty);
		}

		#region Event Handlers
		
		/// <summary>
		/// Called when the name of the node changed.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnNameChanged(EventArgs e)
		{
			if (this.NameChanged != null)
				this.NameChanged(this, e);

			this.OnNodeChanged(EventArgs.Empty);
		}

		/// <summary>
		/// Called when the pin configuration of the node has changed.
		/// This happens with dynamic nodes.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnPinsChanged(EventArgs e)
		{
			if (this.graph != null)
			{
				Input[] inputs = this.GetInputs();
				Connection[] connections = this.GetIncomingConnections();
				foreach (Connection connection in connections)
				{
					if (!((IList)inputs).Contains(connection.Destination))
						this.graph.Disconnect(connection);
				}
				Output[] outputs = this.GetOutputs();
				connections = this.GetOutgoingConnections();
				foreach (Connection connection in connections)
				{
					if (!((IList)outputs).Contains(connection.Source))
						this.graph.Disconnect(connection);
				}
			}

			if (this.PinsChanged != null)
				this.PinsChanged(this, e);

			this.OnNodeChanged(EventArgs.Empty);
		}

		/// <summary>
		/// Called when anything in the node changes.
		/// Includes all other events as well.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnNodeChanged(EventArgs e)
		{
			if (this.NodeChanged != null)
				this.NodeChanged(this, e);

			if (this.graph != null)
				this.graph.OnNodeChanged(e);
		}


		/// <summary>
		/// Called when the graph that owns the node has changed.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnGraphChanged(EventArgs e)
		{
			if (this.GraphChanged != null)
				this.GraphChanged(this, e);
		}

		#endregion
		
		#endregion
	}
}

﻿using System;

namespace NodeGraph
{
	/// <summary>
	/// Marks a field or property as input of a class node.
	/// </summary>
	[AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
	public sealed class InputAttribute : PinAttribute
	{
		#region Fields

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new InputAttribute.
		/// </summary>
		public InputAttribute()
		{

		}

		#endregion

		#region Methods

		#endregion
	}
}

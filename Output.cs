﻿using System;
using System.Collections.Generic;

namespace NodeGraph
{
	/// <summary>
	/// Represents an abstract node output.
	/// </summary>
	public class Output : Pin
	{
		#region Fields

		#endregion

		#region Properties

		public override int Index
		{
			get
			{
				Output[] outputs = this.Node.GetOutputs();
				for (int i = 0; i < outputs.Length; i++)
				{
					if (outputs[i] == this)
						return i;
				}
				throw new Exception("Invalid pin detected.");
			}
		}

		public bool IsCyclic
		{
			get
			{
				return this.Graph.HasCyclicConnection(this);
			}
		}


		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new output.
		/// </summary>
		public Output(Node node, string name, Type type)
			: base(node, name, type)
		{
		}

		#endregion

		#region Methods

		/// <summary>
		/// Connects the output to the specified pin.
		/// </summary>
		/// <param name="destination"></param>
		public Connection Connect(Input destination)
		{
			return this.Graph.Connect(this, destination);
		}

		/// <summary>
		/// Connects the output to multiple pins.
		/// </summary>
		/// <param name="destinations"></param>
		public Connection[] ConnectRange(Input[] destinations)
		{
			Connection[] connections = new Connection[destinations.Length];
			for (int i = 0; i < destinations.Length; i++)
				connections[i] = this.Connect(destinations[i]);
			return connections;
		}

		/// <summary>
		/// Returns an array of connections starting in this output.
		/// </summary>
		/// <returns></returns>
		public Connection[] GetConnections()
		{
			Connection[] connections = this.Node.GetOutgoingConnections();
			List<Connection> myConnections = new List<Connection>();
			foreach (Connection connection in connections)
			{
				if (connection.Source == this)
					myConnections.Add(connection);
			}
			return myConnections.ToArray();
		}

		/// <summary>
		/// Returns an array of pins connected to this output.
		/// </summary>
		/// <returns></returns>
		public Input[] GetDestinations()
		{
			Connection[] connections = this.GetConnections();
			Input[] destinations = new Input[connections.Length];
			for (int i = 0; i < connections.Length; i++)
				destinations[i] = connections[i].Destination;
			return destinations;
		}

		/// <summary>
		/// Disconnects all connections starting in this pin.
		/// </summary>
		public void DisconnectAll()
		{
			Connection[] connections = this.GetConnections();
			foreach (Connection connection in connections)
				this.Node.Graph.Disconnect(connection.Source, connection.Destination);
		}

		#endregion
	}
}

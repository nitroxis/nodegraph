﻿using System;

namespace NodeGraph
{
	/// <summary>
	/// Represents an abtract attribute for pins.
	/// </summary>
	public abstract class PinAttribute : Attribute
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the name of the pin.
		/// </summary>
		public string Name { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new PinAttribute.
		/// </summary>
		protected PinAttribute()
		{

		}

		#endregion

		#region Methods

		#endregion
	}
}

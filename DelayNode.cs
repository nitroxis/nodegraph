﻿namespace NodeGraph
{
	/// <summary>
	/// Represents a node that delays its input by one step.
	/// </summary>
	public class DelayNode<T> : Node
	{
		#region Fields

		private readonly Input input;
		private readonly Output output;

		#endregion

		#region Properties

		public Input Input
		{
			get { return this.input; }
		}

		public Output Output
		{
			get { return this.output; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new DelayNode.
		/// </summary>
		public DelayNode()
		{
			this.input = new Input(this, "Input", typeof(T));
			this.output = new Output(this, "Output", typeof(T));
		}

		#endregion

		#region Methods

		public override Input[] GetInputs()
		{
			return new[] { this.input };
		}

		public override Output[] GetOutputs()
		{
			return new[] { this.output };
		}

		public override void Emit(CodeGenerator generator)
		{
			int index = generator.AddState(typeof(T));
			generator.LoadState(typeof(T), index);
			generator.LoadInput(this.input);
			generator.StoreState(typeof(T), index);
			generator.StoreOutput(this.output);
		}

		#endregion
	}
}

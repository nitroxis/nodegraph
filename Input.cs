﻿using System;

namespace NodeGraph
{
	/// <summary>
	/// Represents an abstract node input.
	/// </summary>
	public class Input : Pin
	{
		#region Fields

		#endregion

		#region Properties

		public override int Index
		{
			get
			{
				Input[] inputs = this.Node.GetInputs();
				for (int i = 0; i < inputs.Length; i++)
				{
					if (inputs[i] == this)
						return i;
				}
				throw new Exception("Invalid pin detected.");
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new input.
		/// </summary>
		public Input(Node node, string name, Type type)
			: base(node, name, type)
		{

		}

		#endregion

		#region Methods

		/// <summary>
		/// Connects the input to the specified source.
		/// </summary>
		/// <param name="source"></param>
		public void Connect(Output source)
		{
			this.Graph.Connect(source, this);
		}

		/// <summary>
		/// Returns the connection that targets this input.
		/// </summary>
		/// <returns></returns>
		public Connection GetConnection()
		{
			Connection[] connections = this.Node.GetIncomingConnections();
			foreach (Connection connection in connections)
			{
				if (connection.Destination == this)
					return connection;
			}
			return null;
		}

		/// <summary>
		/// Returns the source output for this input.
		/// </summary>
		/// <returns></returns>
		public Output GetSource()
		{
			Connection connection = this.GetConnection();
			if (connection != null)
				return connection.Source;
			return null;
		}

		/// <summary>
		/// Disconnects the pin.
		/// </summary>
		/// <returns></returns>
		public bool Disconnect()
		{
			Connection connection = this.GetConnection();
			if (connection != null)
				return this.Node.Graph.Disconnect(connection.Source, connection.Destination);
			return false;
		}

		#endregion
	}
}

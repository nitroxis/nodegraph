﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace NodeGraph
{
	/// <summary>
	/// Represents a node graph containing nodes and connections.
	/// </summary>
	public partial class Graph : Node
	{
		#region Events

		/// <summary>
		/// Fired when a node was added.
		/// </summary>
		public event EventHandler<NodeEventArgs> NodeAdded;
		/// <summary>
		/// Fired when a node was removed.
		/// </summary>
		public event EventHandler<NodeEventArgs> NodeRemoved;
		/// <summary>
		/// Fired when a connection was added.
		/// </summary>
		public event EventHandler<ConnectionEventArgs> ConnectionAdded;
		/// <summary>
		/// Fired when a connection was removed.
		/// </summary>
		public event EventHandler<ConnectionEventArgs> ConnectionRemoved;

		#endregion

		#region Fields

		private readonly List<Node> nodes;
		private readonly List<GraphInput> inputs;
		private readonly List<GraphOutput> outputs;
		private readonly object mutex;
		private readonly List<Connection> connections;

		#endregion

		#region Properties

		/// <summary>
		/// Gets an enumerable of nodes in this graph.
		/// </summary>
		public IEnumerable<Node> Nodes
		{
			get { return this.nodes; }
		}

		/// <summary>
		/// Gets an enumerable of connections in this graph.
		/// </summary>
		public IEnumerable<Connection> Connections
		{
			get { return this.connections; }
		}

		/// <summary>
		/// Gets the graph inputs.
		/// </summary>
		public IEnumerable<GraphInput> Inputs
		{
			get { return this.inputs; }
		}

		/// <summary>
		/// Gets the graph outputs.
		/// </summary>
		public IEnumerable<GraphOutput> Outputs
		{
			get { return this.outputs; }
		}

		/// <summary>
		/// Gets a value that indicates whether this graph is empty, i.e. has no nodes.
		/// </summary>
		public bool IsEmpty
		{
			get { return this.nodes.Count == 0; }
		}

		/// <summary>
		/// Gets a mutex lock.
		/// </summary>
		public object Lock
		{
			get { return this.mutex; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new node graph.
		/// </summary>
		public Graph()
		{
			this.nodes = new List<Node>();
			this.inputs = new List<GraphInput>();
			this.outputs = new List<GraphOutput>();
			this.connections = new List<Connection>();
			this.mutex = new object();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Gets an array of graph inputs.
		/// </summary>
		/// <returns></returns>
		public override Input[] GetInputs()
		{
			//Input[] inputs = new Input[this.inputs.Count];
			//for (int i = 0; i < inputs.Length; i++)
			//	inputs[i] = this.inputs[i];
			//return inputs;
			return this.inputs.ToArray();
		}

		/// <summary>
		/// Gets an array of graph outputs.
		/// </summary>
		/// <returns></returns>
		public override Output[] GetOutputs()
		{
			//Output[] outputs = new Output[this.outputs.Count];
			//for (int i = 0; i < outputs.Length; i++)
			//	outputs[i] = this.outputs[i];
			//return outputs;
			return this.outputs.ToArray();
		}

		/// <summary>
		/// Adds a node to the simulation.
		/// </summary>
		/// <param name="node"></param>
		/// <returns></returns>
		public void AddNode(Node node)
		{
			if (node.Graph != null)
				throw new Exception("Node already in use.");

			node.Graph = this;

			// handle new input node.
			GraphInputNode input = node as GraphInputNode;
			if (input != null && input.GraphPin == null)
			{
				GraphInput pin = new GraphInput(this, input);
				input.GraphPin = pin;
				this.inputs.Add(pin);
				this.OnPinsChanged(EventArgs.Empty);
			}

			// handle new output node.
			GraphOutputNode output = node as GraphOutputNode;
			if (output != null && output.GraphPin == null)
			{
				GraphOutput pin = new GraphOutput(this, output);
				output.GraphPin = pin;
				this.outputs.Add(pin);
				this.OnPinsChanged(EventArgs.Empty);
			}

			// add.
			lock (this.mutex)
				this.nodes.Add(node);

			this.OnNodeAdded(new NodeEventArgs(node));
		}

		/// <summary>
		/// Adds a new node that wraps the specified method.
		/// </summary>
		/// <param name="method"></param>
		/// <returns></returns>
		public Node AddNode(MethodInfo method)
		{
			MethodNode node = new MethodNode(method);
			this.AddNode(node);
			return node;
		}

		/// <summary>
		/// Removes the specified node from the simulation.
		/// </summary>
		/// <param name="node"></param>
		public void RemoveNode(Node node)
		{
			if (this.nodes.Contains(node))
			{
				lock (this.mutex)
					node.DisconnectAll();

				lock (this.mutex)
					this.nodes.Remove(node);

				// handle input node.
				GraphInputNode input = node as GraphInputNode;
				if (input != null && this.inputs.Contains(input.GraphPin))
				{
					this.inputs.Remove(input.GraphPin);
					this.OnPinsChanged(EventArgs.Empty);
					input.GraphPin = null;
				}

				// handle output node.
				GraphOutputNode output = node as GraphOutputNode;
				if (output != null && this.outputs.Contains(output.GraphPin))
				{
					this.outputs.Remove(output.GraphPin);
					this.OnPinsChanged(EventArgs.Empty);
					output.GraphPin = null;
				}

				node.Graph = null;
				
				this.OnNodeRemoved(new NodeEventArgs(node));
			}
		}

		/// <summary>
		/// Removes all nodes from the graph.
		/// </summary>
		public void Clear()
		{
			while (this.nodes.Count > 0)
			{
				this.RemoveNode(this.nodes[0]);
			}
		}

		/// <summary>
		/// Checks whether the specified node is added to this graph.
		/// </summary>
		/// <param name="node"></param>
		/// <returns></returns>
		public bool ContainsNode(Node node)
		{
			return node.Graph == this;
		}

		/// <summary>
		/// Gets the index of a node.
		/// </summary>
		/// <param name="node"></param>
		/// <returns></returns>
		public int GetNodeIndex(Node node)
		{
			return this.nodes.IndexOf(node);
		}

		/// <summary>
		/// Gets the node at the specified index.
		/// </summary>
		/// <param name="index"></param>
		/// <returns></returns>
		public Node GetNodeAt(int index)
		{
			return this.nodes[index];
		}

		/// <summary>
		/// Adds a new graph input.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="type"></param>
		/// <returns></returns>
		public GraphInput AddInput(string name, Type type)
		{
			GraphInput pin = new GraphInput(this, name, type);
			this.inputs.Add(pin);
			this.OnPinsChanged(EventArgs.Empty);
			this.AddNode(pin.PinNode);
			return pin;
		}

		/// <summary>
		/// Removes an input from the graph.
		/// </summary>
		/// <param name="input"></param>
		public void RemoveInput(GraphInput input)
		{
			if (!this.inputs.Remove(input))
				throw new Exception("Input does not belong to graph.");
			this.OnPinsChanged(EventArgs.Empty);
			this.RemoveNode(input.PinNode);
		}

		/// <summary>
		/// Adds a new graph output.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="type"></param>
		/// <returns></returns>
		public GraphOutput AddOutput(string name, Type type)
		{
			GraphOutput pin = new GraphOutput(this, name, type);
			this.outputs.Add(pin);
			this.OnPinsChanged(EventArgs.Empty);
			this.AddNode(pin.PinNode);
			return pin;
		}

		/// <summary>
		/// Removes an output from the graph.
		/// </summary>
		/// <param name="output"></param>
		public void RemoveOutput(GraphOutput output)
		{
			if (!this.outputs.Remove(output))
				throw new Exception("Output does not belong to graph.");
			this.OnPinsChanged(EventArgs.Empty);
			this.RemoveNode(output.PinNode);
		}

		/// <summary>
		/// Connects an output to an input.
		/// </summary>
		/// <param name="source">The source output.</param>
		/// <param name="destination">The destination input.</param>
		public Connection Connect(Output source, Input destination)
		{
			Connection connection = new Connection(source, destination);
			destination.Disconnect();
			lock (this.mutex)
				this.connections.Add(connection);
			this.OnConnectionAdded(new ConnectionEventArgs(connection));
			return connection;
		}

		/// <summary>
		/// Disconnects the two specified pins.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="destination"></param>
		/// <returns></returns>
		public bool Disconnect(Output source, Input destination)
		{
			foreach (Connection connection in this.connections)
			{
				if (connection.Source == source && connection.Destination == destination)
				{
					lock (this.mutex)
						this.connections.Remove(connection);
					this.OnConnectionRemoved(new ConnectionEventArgs(connection));
					return true;
				}
			}
			return false;
		}

		/// <summary>
		/// Removes the specified connection.
		/// </summary>
		/// <returns></returns>
		public bool Disconnect(Connection connection)
		{
			return this.Disconnect(connection.Source, connection.Destination);
		}

		/// <summary>
		/// Returns the connection ends in the specified input.
		/// </summary>
		/// <param name="node"></param>
		/// <param name="input"></param>
		/// <returns></returns>
		public Connection GetSource(Node node, Input input)
		{
			foreach (Connection connection in this.connections)
			{
				if (connection.DestinationNode == node && connection.Destination == input)
					return connection;
			}
			return null;
		}

		/// <summary>
		/// Returns all connections that end in the specified node.
		/// </summary>
		/// <param name="node"></param>
		/// <returns></returns>
		public Connection[] GetIncomingConnections(Node node)
		{
			List<Connection> incomingConnections = new List<Connection>();
			foreach (Connection connection in this.connections)
			{
				if (connection.DestinationNode == node)
					incomingConnections.Add(connection);
			}
			return incomingConnections.ToArray();
		}

		/// <summary>
		/// Returns the connections that start at the specified output.
		/// </summary>
		/// <param name="node"></param>
		/// <param name="output"></param>
		/// <returns></returns>
		public Connection[] GetDestinations(Node node, Output output)
		{
			List<Connection> outputConnections = new List<Connection>();
			foreach (Connection connection in this.connections)
			{
				if (connection.SourceNode == node && connection.Source == output)
					outputConnections.Add(connection);
			}
			return outputConnections.ToArray();
		}

		/// <summary>
		/// Returns all connections that start in the specified instance.
		/// </summary>
		/// <param name="node"></param>
		/// <returns></returns>
		public Connection[] GetOutgoingConnections(Node node)
		{
			List<Connection> outgoingConnections = new List<Connection>();
			foreach (Connection connection in this.connections)
			{
				if (connection.SourceNode == node)
					outgoingConnections.Add(connection);
			}
			return outgoingConnections.ToArray();
		}

		/// <summary>
		/// Returns all connections that either start or end in the specified node.
		/// </summary>
		/// <param name="node"></param>
		/// <returns></returns>
		public Connection[] GetAllConnections(Node node)
		{
			List<Connection> outgoingConnections = new List<Connection>();
			foreach (Connection connection in this.connections)
			{
				if (connection.SourceNode == node || connection.DestinationNode == node)
					outgoingConnections.Add(connection);
			}
			return outgoingConnections.ToArray();
		}

		/// <summary>
		/// Returns the node instances that the specified instance depends on.
		/// </summary>
		/// <param name="node"></param>
		/// <returns></returns>
		public Node[] GetDependencies(Node node)
		{
			List<Node> dependencies = new List<Node>();
			foreach (Connection connection in this.connections)
			{
				if (connection.DestinationNode == node && !dependencies.Contains(connection.SourceNode))
					dependencies.Add(connection.SourceNode);
			}
			return dependencies.ToArray();
		}

		/// <summary>
		/// Checks all nodes for cyclic connections.
		/// </summary>
		/// <returns></returns>
		public bool HasCyclicConnection()
		{
			foreach (Node node in this.nodes)
			{
				if (this.HasCyclicConnection(node))
					return true;
			}

			return false;
		}

		/// <summary>
		/// Checks whether the node has any cyclic connection.
		/// </summary>
		/// <param name="node"></param>
		/// <returns></returns>
		public bool HasCyclicConnection(Node node)
		{
			return this.searchNode(node, node, new List<Node>());
		}

		/// <summary>
		/// Checks whether the output has a cyclic connection.
		/// </summary>
		/// <param name="output"></param>
		/// <returns></returns>
		public bool HasCyclicConnection(Output output)
		{
			List<Node> checklist = new List<Node>();
			foreach (Connection connection in output.GetConnections())
			{
				if (this.searchOutput(output.Node, connection.DestinationNode, checklist))
					return true;
			}
			return false;
		}

		private bool searchNode(Node wanted, Node current, List<Node> checklist)
		{
			checklist.Add(current);

			Node[] deps = this.GetDependencies(current);
			
			foreach (Node node in deps)
				if (node == wanted) return true;

			foreach (Node node in deps)
			{
				if (!checklist.Contains(node))
				{
					if (this.searchNode(wanted, node, checklist))
						return true;
				}
			}

			return false;
		}

		private bool searchOutput(Node wanted, Node current, List<Node> checklist)
		{
			if (current == wanted)
				return true;

			if (checklist.Contains(current))
				return false;

			checklist.Add(current);
	
			foreach (Connection connection in current.GetOutgoingConnections())
			{
				if (this.searchOutput(wanted, connection.DestinationNode, checklist))
					return true;
			}
		
			return false;
		}


		#region Event Handlers


		/// <summary>
		/// Called when a node is added to the graph.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnNodeAdded(NodeEventArgs e)
		{
			if (this.NodeAdded != null)
				this.NodeAdded(this, e);

			this.OnNodeChanged(EventArgs.Empty);
		}

		/// <summary>
		/// Called when a node is removed from the graph.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnNodeRemoved(NodeEventArgs e)
		{
			if (this.NodeRemoved != null)
				this.NodeRemoved(this, e);

			this.OnNodeChanged(EventArgs.Empty);
		}

		/// <summary>
		/// Called when a new connection is added.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnConnectionAdded(ConnectionEventArgs e)
		{
			if (this.ConnectionAdded != null)
				this.ConnectionAdded(this, e);

			this.OnNodeChanged(EventArgs.Empty);
		}

		/// <summary>
		/// Called when a connection is removed.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnConnectionRemoved(ConnectionEventArgs e)
		{
			if (this.ConnectionRemoved != null)
				this.ConnectionRemoved(this, e);

			this.OnNodeChanged(EventArgs.Empty);
		}

		#endregion

		#endregion
	}
}

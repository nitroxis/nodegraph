﻿using System;
using System.Reflection;

namespace NodeGraph
{
	/// <summary>
	/// Internal node graph state.
	/// </summary>
	public sealed class GraphState : ICloneable
	{
		#region Constants

		private const int initialSize = 0;
		private const int resizeIncrement = 4;

		#endregion

		#region Fields

		private GraphState backup;

		public static readonly FieldInfo BooleanField = typeof(GraphState).GetField("Boolean", BindingFlags.Public | BindingFlags.Instance);
		public bool[] Boolean;
		private int booleanIndex;

		public static readonly FieldInfo Int8Field = typeof(GraphState).GetField("Int8", BindingFlags.Public | BindingFlags.Instance);
		public sbyte[] Int8;
		private int int8Index;

		public static readonly FieldInfo UInt8Field = typeof(GraphState).GetField("UInt8", BindingFlags.Public | BindingFlags.Instance);
		public byte[] UInt8;
		private int uint8Index;

		public static readonly FieldInfo Int16Field = typeof(GraphState).GetField("Int16", BindingFlags.Public | BindingFlags.Instance);
		public short[] Int16;
		private int int16Index;

		public static readonly FieldInfo UInt16Field = typeof(GraphState).GetField("UInt16", BindingFlags.Public | BindingFlags.Instance);
		public ushort[] UInt16;
		private int uint16Index;

		public static readonly FieldInfo Int32Field = typeof(GraphState).GetField("Int32", BindingFlags.Public | BindingFlags.Instance);
		public int[] Int32;
		private int int32Index;

		public static readonly FieldInfo UInt32Field = typeof(GraphState).GetField("UInt32", BindingFlags.Public | BindingFlags.Instance);
		public uint[] UInt32;
		private int uint32Index;

		public static readonly FieldInfo Int64Field = typeof(GraphState).GetField("Int64", BindingFlags.Public | BindingFlags.Instance);
		public long[] Int64;
		private int int64Index;

		public static readonly FieldInfo UInt64Field = typeof(GraphState).GetField("UInt64", BindingFlags.Public | BindingFlags.Instance);
		public ulong[] UInt64;
		private int uint64Index;

		public static readonly FieldInfo SingleField = typeof(GraphState).GetField("Single", BindingFlags.Public | BindingFlags.Instance);
		public float[] Single;
		private int singleIndex;

		public static readonly FieldInfo DoubleField = typeof(GraphState).GetField("Double", BindingFlags.Public | BindingFlags.Instance);
		public double[] Double;
		private int doubleIndex;

		public static readonly FieldInfo CharField = typeof(GraphState).GetField("Char", BindingFlags.Public | BindingFlags.Instance);
		public char[] Char;
		private int charIndex;

		public static readonly FieldInfo StringField = typeof(GraphState).GetField("String", BindingFlags.Public | BindingFlags.Instance);
		public string[] String;
		private int stringIndex;

		public static readonly FieldInfo ObjectField = typeof(GraphState).GetField("Object", BindingFlags.Public | BindingFlags.Instance);
		public object[] Object;
		private int objectIndex;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new State.
		/// </summary>
		public GraphState()
		{
			this.Boolean = new bool[initialSize];
			this.Int8 = new sbyte[initialSize];
			this.UInt8 = new byte[initialSize];
			this.Int16 = new short[initialSize];
			this.UInt16 = new ushort[initialSize];
			this.Int32 = new int[initialSize];
			this.UInt32 = new uint[initialSize];
			this.Int64 = new long[initialSize];
			this.UInt64 = new ulong[initialSize];
			this.Single = new float[initialSize];
			this.Double = new double[initialSize];
			this.Char = new char[initialSize];
			this.String = new string[initialSize];
			this.Object = new object[initialSize];
		}

		public GraphState(GraphState state)
		{
			GraphState.copy(state, this);
		}

		#endregion

		#region Methods

		public int AddBoolean(bool value)
		{
			if (this.booleanIndex >= this.Boolean.Length)
				Array.Resize(ref this.Boolean, this.Boolean.Length + resizeIncrement);

			this.Boolean[this.booleanIndex] = value;
			return this.booleanIndex++;
		}

		public int AddInt8(sbyte value)
		{
			if (this.int8Index >= this.Int8.Length)
				Array.Resize(ref this.Int8, this.Int8.Length + resizeIncrement);

			this.Int8[this.int8Index] = value;
			return this.int8Index++;
		}

		public int AddUInt8(byte value)
		{
			if (this.uint8Index >= this.UInt8.Length)
				Array.Resize(ref this.UInt8, this.UInt8.Length + resizeIncrement);

			this.UInt8[this.uint8Index] = value;
			return this.uint8Index++;
		}

		public int AddInt16(short value)
		{
			if (this.int16Index >= this.Int16.Length)
				Array.Resize(ref this.Int16, this.Int16.Length + resizeIncrement);

			this.Int16[this.int16Index] = value;
			return this.int16Index++;
		}

		public int AddUInt16(ushort value)
		{
			if (this.uint16Index >= this.UInt16.Length)
				Array.Resize(ref this.UInt16, this.UInt16.Length + resizeIncrement);

			this.UInt16[this.uint16Index] = value;
			return this.uint16Index++;
		}

		public int AddInt32(int value)
		{
			if(this.int32Index >= this.Int32.Length)
				Array.Resize(ref this.Int32, this.Int32.Length + resizeIncrement);

			this.Int32[this.int32Index] = value;
			return this.int32Index++;
		}


		public int AddUInt32(uint value)
		{
			if (this.uint32Index >= this.UInt32.Length)
				Array.Resize(ref this.UInt32, this.UInt32.Length + resizeIncrement);

			this.UInt32[this.uint32Index] = value;
			return this.uint32Index++;
		}

		public int AddInt64(long value)
		{
			if (this.int64Index >= this.Int64.Length)
				Array.Resize(ref this.Int64, this.Int64.Length + resizeIncrement);

			this.Int64[this.int64Index] = value;
			return this.int64Index++;
		}

		public int AddUInt64(ulong value)
		{
			if (this.uint64Index >= this.UInt64.Length)
				Array.Resize(ref this.UInt64, this.UInt64.Length + resizeIncrement);

			this.UInt64[this.uint64Index] = value;
			return this.uint64Index++;
		}

		public int AddSingle(uint value)
		{
			if (this.singleIndex >= this.Single.Length)
				Array.Resize(ref this.Single, this.Single.Length + resizeIncrement);

			this.Single[this.singleIndex] = value;
			return this.singleIndex++;
		}

		public int AddDouble(long value)
		{
			if (this.doubleIndex >= this.Double.Length)
				Array.Resize(ref this.Double, this.Double.Length + resizeIncrement);

			this.Double[this.doubleIndex] = value;
			return this.doubleIndex++;
		}

		public int AddChar(char value)
		{
			if (this.charIndex >= this.Char.Length)
				Array.Resize(ref this.Char, this.Char.Length + resizeIncrement);

			this.Char[this.charIndex] = value;
			return this.charIndex++;
		}

		public int AddString(string value)
		{
			if (this.stringIndex >= this.String.Length)
				Array.Resize(ref this.String, this.String.Length + resizeIncrement);

			this.String[this.stringIndex] = value;
			return this.stringIndex++;
		}

		public int AddObject(object value)
		{
			if (this.objectIndex >= this.Object.Length)
				Array.Resize(ref this.Object, this.Object.Length + resizeIncrement);

			this.Object[this.objectIndex] = value;
			return this.objectIndex++;
		}

		public void Optimize()
		{
			Array.Resize(ref this.Boolean, this.booleanIndex);
			Array.Resize(ref this.Int8, this.int8Index);
			Array.Resize(ref this.UInt8, this.uint8Index);
			Array.Resize(ref this.Int16, this.int16Index);
			Array.Resize(ref this.UInt16, this.uint16Index);
			Array.Resize(ref this.Int32, this.int32Index);
			Array.Resize(ref this.UInt32, this.uint32Index);
			Array.Resize(ref this.Int64, this.int64Index);
			Array.Resize(ref this.UInt64, this.uint64Index);
			Array.Resize(ref this.Single, this.singleIndex);
			Array.Resize(ref this.Double, this.doubleIndex);
			Array.Resize(ref this.Char, this.charIndex);
			Array.Resize(ref this.String, this.stringIndex);
			Array.Resize(ref this.Object, this.objectIndex);
		}

		public void Backup()
		{
			this.backup = this.Clone();
		}

		public void Restore()
		{
			GraphState.copy(this.backup, this);
		}

		private static void copy(GraphState src, GraphState dest)
		{
			dest.Boolean = new bool[src.Boolean.Length];
			dest.Int8 = new sbyte[src.Int8.Length];
			dest.UInt8 = new byte[src.UInt8.Length];
			dest.Int16 = new short[src.Int16.Length];
			dest.UInt16 = new ushort[src.UInt16.Length];
			dest.Int32 = new int[src.Int32.Length];
			dest.UInt32 = new uint[src.UInt32.Length];
			dest.Int64 = new long[src.Int64.Length];
			dest.UInt64 = new ulong[src.UInt64.Length];
			dest.Single = new float[src.Single.Length];
			dest.Double = new double[src.Double.Length];
			dest.Char = new char[src.Char.Length];
			dest.String = new string[src.String.Length];
			dest.Object = new object[src.Object.Length];

			Array.Copy(src.Boolean, dest.Boolean, src.Boolean.Length);
			Array.Copy(src.Int8, dest.Int8, src.Int8.Length);
			Array.Copy(src.UInt8, dest.UInt8, src.UInt8.Length);
			Array.Copy(src.Int16, dest.Int16, src.Int16.Length);
			Array.Copy(src.UInt16, dest.UInt16, src.UInt16.Length);
			Array.Copy(src.Int32, dest.Int32, src.Int32.Length);
			Array.Copy(src.UInt32, dest.UInt32, src.UInt32.Length);
			Array.Copy(src.Int64, dest.Int64, src.Int64.Length);
			Array.Copy(src.UInt64, dest.UInt64, src.UInt64.Length);
			Array.Copy(src.Single, dest.Single, src.Single.Length);
			Array.Copy(src.Double, dest.Double, src.Double.Length);
			Array.Copy(src.Char, dest.Char, src.Char.Length);
			Array.Copy(src.String, dest.String, src.String.Length);
			Array.Copy(src.Object, dest.Object, src.Object.Length);

			dest.booleanIndex = src.booleanIndex;
			dest.int8Index = src.int8Index;
			dest.uint8Index = src.uint8Index;
			dest.int16Index = src.int16Index;
			dest.uint16Index = src.uint16Index;
			dest.int32Index = src.int32Index;
			dest.uint32Index = src.uint32Index;
			dest.int64Index = src.int64Index;
			dest.uint64Index = src.uint64Index;
			dest.singleIndex = src.singleIndex;
			dest.doubleIndex = src.doubleIndex;
			dest.charIndex = src.charIndex;
			dest.stringIndex = src.stringIndex;
			dest.objectIndex = src.objectIndex;
		}

		public GraphState Clone()
		{
			return new GraphState(this);
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		#endregion
	}
}

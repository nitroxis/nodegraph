﻿using System.Reflection.Emit;

namespace NodeGraph
{
	/// <summary>
	/// Represents a ternary operator that either returns the first or the second input depending on the boolean valve of a third input.
	/// </summary>
	public sealed class TernaryNode<T> : Node
	{
		#region Fields

		private readonly Input booleanInput;
		private readonly Input trueInput;
		private readonly Input falseInput;
		private readonly Output output;

		#endregion

		#region Properties

		public Input BooleanInput
		{
			get { return this.booleanInput; }
		}

		public Input TrueInput
		{
			get { return this.trueInput; }
		}

		public Input FalseInput
		{
			get { return this.falseInput; }
		}

		public Output Output
		{
			get { return this.output; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new TernaryNode.
		/// </summary>
		public TernaryNode()
		{
			this.booleanInput = new Input(this, "Condition", typeof(bool));
			this.trueInput = new Input(this, "True", typeof(T));
			this.falseInput = new Input(this, "False", typeof(T));
			this.output = new Output(this, "Output", typeof(T));
		}

		#endregion

		#region Methods

		public override Input[] GetInputs()
		{
			return new Input[] {this.booleanInput, this.trueInput, this.falseInput};
		}

		public override Output[] GetOutputs()
		{
			return new Output[] {this.output};
		}

		public override void Emit(CodeGenerator generator)
		{
			Label falseLabel = generator.IL.DefineLabel();
			Label endLabel = generator.IL.DefineLabel();
			
			generator.LoadInput(this.booleanInput);
			generator.IL.Emit(OpCodes.Brfalse, falseLabel);

			generator.LoadInput(this.trueInput);
			generator.StoreOutput(this.output);
			generator.IL.Emit(OpCodes.Br_S, endLabel);

			generator.IL.MarkLabel(falseLabel);
			generator.LoadInput(this.falseInput);
			generator.StoreOutput(this.output);

			generator.IL.MarkLabel(endLabel);
		}

		#endregion
	}
}
